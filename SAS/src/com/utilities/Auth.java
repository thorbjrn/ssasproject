package com.utilities;

import java.io.Serializable;

public class Auth implements Serializable {
	private static final long serialVersionUID = -8498484974365604135L;

	/**
	 * Auth configuration
	 */
	private static String _hash_key = "O3HfU9UFgBPaGEgLcO6ig6SUt3aVB8RFcNhfBTrSJqXWn7iaFc9sT0UtMrmiJ0y";

	public Auth() {

	}

	public static String hash_password(String password) {
		return Hasher.toSHA256(password + _hash_key);
	}
}
