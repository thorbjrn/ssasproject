package com.utilities;

import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.activation.DataHandler;
import javax.mail.*;
import javax.mail.internet.*;
import javax.mail.util.ByteArrayDataSource;

import com.classes.User;
import com.sun.mail.smtp.SMTPTransport;

public class Mail {
	static final String fromMail = "itugroup5email@gmail.com";

	/**
	 * Simple mailing service:)
	 * 
	 * @param bodyHTML
	 *            The HTML that should be included in the body of the email
	 * @param recipient
	 *            The recipient of the mail
	 */
	public static void sendEmail(DataHandler dataHandler, String subject, String recipient) {
		try {
			Logger.getLogger(Mail.class.getName()).log(Level.INFO,
					"start sending conf mail to: " + recipient);
			
			Properties props = System.getProperties();
			props.put("mail.smtps.host", "smtp.gmail.com");
			props.put("mail.smtps.auth", "true");
			
			javax.mail.Session session = javax.mail.Session.getInstance(props,
					null);
			
			Message msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress(fromMail));
			;
			msg.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(recipient, false));
			msg.setSubject(subject);
			msg.setDataHandler(dataHandler);
//			msg.setContent(bodyHTML, "text/html");
//			msg.setText(bodyHTML);
			msg.setHeader("X-Mailer", "ITUF");
			msg.setSentDate(new Date());
			
			SMTPTransport t = (SMTPTransport) session.getTransport("smtps");
			t.connect("smtp.gmail.com", fromMail, "wHuVFMvMBl");
			t.sendMessage(msg, msg.getAllRecipients());
			System.out.println("Response: " + t.getLastServerResponse());
			t.close();
		} catch (AddressException e) {
			Logger.getLogger(Mail.class.getName()).log(Level.SEVERE,
					"Error in sending mail to recipient: " + recipient, e);
		} catch (NoSuchProviderException e) {
			Logger.getLogger(Mail.class.getName()).log(Level.SEVERE,
					"Error in the SMTP provider", e);
		} catch (MessagingException e) {
			Logger.getLogger(Mail.class.getName()).log(Level.SEVERE,
					"Error in creating the mail to recipient: " + recipient, e);
		}
	}

	public static void sendActivationEmail(User user) throws Exception {
		
		String recipient = user.getUser_email();
		
		String url = "https://192.237.211.45/ssasf14/activate.xhtml";
		url += "?user=" + Encrypt.encode(user.getUser_email());
		url += "&code=" + user.getActivation_code();
		
		StringBuffer sb = new StringBuffer();
		sb.append("<table style=\"font-family: sans-serif;\"><tbody>");
		sb.append("<tr><td style=\"background: #2e2e2e; color: #ffffff; text-align: center; padding: 20px; font-size: 1.5em;\">");
		sb.append("Welcome to Meeting site by <b><i>Team5</i></b>");
		sb.append("</td></tr>");
		sb.append("<tr><td style=\"height: 5px; background-color: #49CBCD;\"></td></tr>");
		sb.append("<tr><td style=\"height: 30px;\"></td></tr>");
		sb.append("<tr><td style=\"padding: 0 10px;\">");
		sb.append("Hello, <b>"+user.getFirst_name()+"</b>");
		sb.append("</td></tr>");
		sb.append("<tr><td style=\"padding: 0 10px;\">");
		sb.append("An account for you has been created, but we ask you to verify your email before using it.");
		sb.append("</td></tr>");
		sb.append("<tr><td style=\"padding: 0 10px;\">");
		sb.append("You can do it by going to this link:");
		sb.append("</td></tr>");
		sb.append("<tr><td style=\"padding: 0 10px;\">");
		sb.append("<a href=\""+url+"\">"+url+"</a>");
		sb.append("</td></tr>");
		sb.append("<tr><td style=\"height: 30px;\"></td></tr>");
		sb.append("<tr><td style=\"padding: 10px 0; background-color: #49CBCD; color: #ffffff; text-align: center;\">");
		sb.append("Group5 © 2014");
		sb.append("</td></tr>");
		sb.append("</tbody></table>");
		
		sendEmail(new DataHandler(new ByteArrayDataSource(sb.toString(), "text/html")), "Welcome to Group 5's friends website ", recipient);
	}


public static void sendPasswordResetEmail(User user) throws Exception { //also creates a new user.resetcode.
	
	String recipient = user.getUser_email();
	
	String resetUrl =  "https://192.237.211.45/ssasf14/reset.xhtml";
	resetUrl += "?user=" + Encrypt.encode(user.getUser_email());
	resetUrl += "&resetCode=" + user.getResetCode();
	
	StringBuffer sb = new StringBuffer();
	sb.append("<table style=\"font-family: sans-serif; width: 600px;\"><tbody>");
	sb.append("<tr><td style=\"background: #2e2e2e; color: #ffffff; text-align: center; padding: 20px; font-size: 1.5em;\">");
	sb.append("Password reset request for Meeting site by <b><i>Team5</i></b>");
	sb.append("</td></tr>");
	sb.append("<tr><td style=\"height: 5px; background-color: #49CBCD;\"></td></tr>");
	sb.append("<tr><td style=\"height: 30px;\"></td></tr>");
	sb.append("<tr><td style=\"padding: 0 10px;\">");
	sb.append("Hello, <b>"+user.getFirst_name()+"</b>");
	sb.append("</td></tr>");
	sb.append("<tr><td style=\"padding: 0 10px;\">");
	sb.append("A new password has been requested for your account.");
	sb.append("</td></tr>");
	sb.append("<tr><td style=\"padding: 0 10px;\">");
	sb.append("Click the following link to get a new password for your account:");
	sb.append("</td></tr>");
	sb.append("<tr><td style=\"padding: 0 10px; width: 600px; word-wrap:break-word;\">");
	sb.append("<a href=\""+resetUrl+"\">"+resetUrl+"</a>");
	sb.append("</td></tr>");
	sb.append("<tr><td style=\"height: 30px;\"></td></tr>");
	sb.append("<tr><td style=\"padding: 10px 0; background-color: #49CBCD; color: #ffffff; text-align: center;\">");
	sb.append("Group5 © 2014");
	sb.append("</td></tr>");
	sb.append("</tbody></table>");
	
	sendEmail(new DataHandler(new ByteArrayDataSource(sb.toString(), "text/html")), "Forgot our password, did we? ", recipient);
}
}

/*
 * package dk.itu.friends.utilities;
 * 
 * import java.util.Date; import java.util.Properties; import
 * java.util.logging.Level; import java.util.logging.Logger;
 * 
 * import javax.mail.*; import javax.mail.internet.*;
 * 
 * import com.sun.mail.smtp.SMTPTransport;
 * 
 * public class Mail { }
 */