package com.utilities;

import java.io.Serializable;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

/**
 * 
 * @author Ignas Jonikavicius
 */
public class Encrypt implements Serializable {
	private static final long serialVersionUID = 4477576656203164490L;

	// must be 8 char long
	private final static String SECRETKEY = "iryhMnVB";

	// Algorithm used
	private final static String ALGORITHM = "DES";

	/**
	 * Encrypt data
	 * 
	 * @param data
	 *            - data to encrypt
	 * @return Encrypted data
	 * @throws Exception
	 */
	public static String encode(String data) throws Exception {
		return Encrypt.encode(SECRETKEY, data);
	}

	/**
	 * Encrypt data
	 * 
	 * @param secretKey
	 *            - a secret key used for encryption. Must be 8 char long
	 * @param data
	 *            - data to encrypt
	 * @return Encrypted data
	 * @throws Exception
	 */
	public static String encode(String secretKey, String data) throws Exception {
		// Key has to be of length 8
		if (secretKey == null || secretKey.length() != 8) {
			secretKey = SECRETKEY;
		}

		SecretKeySpec key = new SecretKeySpec(secretKey.getBytes(), ALGORITHM);
		Cipher cipher = Cipher.getInstance(ALGORITHM);
		cipher.init(Cipher.ENCRYPT_MODE, key);

		return toHex(cipher.doFinal(data.getBytes()));
	}

	/**
	 * Decrypt data
	 * 
	 * @param data
	 *            - data to decrypt
	 * @return Decrypted data
	 * @throws Exception
	 */
	public static String decode(String data) throws Exception {
		return Encrypt.decode(SECRETKEY, data);
	}

	/**
	 * Decrypt data
	 * 
	 * @param secretKey
	 *            - a secret key used for decryption. Must be 8 char long
	 * @param data
	 *            - data to decrypt
	 * @return Decrypted data
	 * @throws Exception
	 */
	public static String decode(String secretKey, String data) throws Exception {
		// Key has to be of length 8
		if (secretKey == null || secretKey.length() != 8) {
			secretKey = SECRETKEY;
		}

		SecretKeySpec key = new SecretKeySpec(secretKey.getBytes(), ALGORITHM);
		Cipher cipher = Cipher.getInstance(ALGORITHM);
		cipher.init(Cipher.DECRYPT_MODE, key);

		return new String(cipher.doFinal(toByte(data)));
	}

	// Helper methods
	private static byte[] toByte(String hexString) {
		int len = hexString.length() / 2;

		byte[] result = new byte[len];

		for (int i = 0; i < len; i++) {
			result[i] = Integer.valueOf(hexString.substring(2 * i, 2 * i + 2),
					16).byteValue();
		}
		return result;
	}

	public static String toHex(byte[] stringBytes) {
		StringBuffer result = new StringBuffer(2 * stringBytes.length);

		for (int i = 0; i < stringBytes.length; i++) {
			result.append(HEX.charAt((stringBytes[i] >> 4) & 0x0f)).append(
					HEX.charAt(stringBytes[i] & 0x0f));
		}

		return result.toString();
	}

	private final static String HEX = "0123456789ABCDEF";

	// Helper methods - end
}
