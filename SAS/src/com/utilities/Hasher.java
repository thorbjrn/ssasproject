package com.utilities;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 
 * @author Ignas Jonikavicius
 */
public class Hasher implements Serializable {
	private static final long serialVersionUID = -6659946424075808445L;

	/**
	 * Hashed the provided text using SHA-256
	 * 
	 * @param text
	 *            String to hash
	 * @return text hash
	 */
	public static String toSHA256(String text) {
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			md.update(text.getBytes("UTF-8"));
			byte[] digest = md.digest();
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < digest.length; i++) {
				sb.append(Integer.toString((digest[i] & 0xff) + 0x100, 16)
						.substring(1));
			}
			return sb.toString();
		} catch (NoSuchAlgorithmException ex) {
			Logger.getLogger(Hasher.class.getName())
					.log(Level.SEVERE, null, ex);
			return null;
		} catch (UnsupportedEncodingException e) {
			Logger.getLogger(Hasher.class.getName()).log(Level.SEVERE, null, e);
			return null;
		}
	}

	/**
	 * Hashed the provided text using SHA-1
	 * 
	 * @param text
	 *            String to hash
	 * @return text hash
	 */
	public static String toSHA1(String text) {
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-1");
			md.update(text.getBytes("UTF-8"));
			byte[] digest = md.digest();
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < digest.length; i++) {
				sb.append(Integer.toString((digest[i] & 0xff) + 0x100, 16)
						.substring(1));
			}
			return sb.toString();
		} catch (NoSuchAlgorithmException ex) {
			Logger.getLogger(Hasher.class.getName())
					.log(Level.SEVERE, null, ex);
			return null;
		} catch (UnsupportedEncodingException e) {
			Logger.getLogger(Hasher.class.getName()).log(Level.SEVERE, null, e);
			return null;
		}
	}
}
