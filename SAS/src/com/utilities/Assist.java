package com.utilities;

import java.io.IOException;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

public class Assist {

	//
	// Errors
	//

	protected void throwError404() {
		throwError(404);
	}

	protected void throwError500() {
		throwError(500);
	}

	protected void throwError(int errorCode) {
		FacesContext fc = FacesContext.getCurrentInstance();
		ExternalContext efc = fc.getExternalContext();

		try {
			efc.responseSendError(errorCode, "");
		} catch (IOException e) {
			System.out.println("Fail in throwing error: " + errorCode);
			System.out.println(e.getMessage());
		}
	}

	//
	// Parameters
	//

	protected String getParam(String name) {
		FacesContext fc = FacesContext.getCurrentInstance();
		ExternalContext efc = fc.getExternalContext();
		Map<String, String> params = efc.getRequestParameterMap();

		return params.get(name);
	}

	//
	// MESSAGES
	//

	protected void showDbError() {
		showError("Database error",
				"We are sorry, but an error occurred during your last request.");
		showError("Database error",
				"Please try again or contact system administration if error persists.");
	}

	protected void showError(String summary, String detail) {
		showMessage(FacesMessage.SEVERITY_ERROR, summary, detail);
	}

	protected void showInfo(String summary, String detail) {
		showMessage(FacesMessage.SEVERITY_INFO, summary, detail);
	}

	protected void showWarning(String summary, String detail) {
		showMessage(FacesMessage.SEVERITY_WARN, summary, detail);
	}

	protected void showMessage(Severity severity, String summary, String detail) {
		FacesContext fc = FacesContext.getCurrentInstance();
		fc.addMessage(null, new FacesMessage(severity, summary, detail));
	}

	/**
	 * Persist message during redirect
	 */
	protected void persistMessage() {
		FacesContext fc = FacesContext.getCurrentInstance();
		ExternalContext efc = fc.getExternalContext();
		efc.getFlash().setKeepMessages(true);
	}
}
