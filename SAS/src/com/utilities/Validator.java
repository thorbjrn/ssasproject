package com.utilities;

import java.io.Serializable;

/**
 * 
 * @author Ignas Jonikavicius
 */
public class Validator implements Serializable {
	private static final long serialVersionUID = -2296423418226120091L;

	private static Validator instance; // Singleton

	public Validator() {
	}

	public static Validator getInstance() { // Singleton
		if (instance == null)
			instance = new Validator();
		return instance;
	}

	public boolean validateSignup(String username, String password,
			String repeatPassword) {
		if (username == null || password == null || repeatPassword == null)
			return false; // return error message

		if (!password.equals(repeatPassword))
			return false; // return error message

		if (username.split("@").length == 2 && username.split("@")[1] != null) {
			String domain = username.split("@")[1];
			return domain.equalsIgnoreCase("itu.dk")
					|| domain.equalsIgnoreCase("itu.com");
		}

		return false;
	}
}
