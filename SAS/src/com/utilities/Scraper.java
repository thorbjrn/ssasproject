package com.utilities;
import java.net.*;
import java.security.KeyManagementException;
import java.io.*;
import javax.net.ssl.*;

public class Scraper {

	public Scraper(){

	}

	public String ScrapeURL(String serviceIP) throws Exception{
		
		class trust implements HostnameVerifier
		{
			public boolean verify(String hostname,
		             SSLSession session)
			{
				return true;
			}
		}
		// Create a trust manager that does not validate certificate chains
		TrustManager[] trustAllCerts = new TrustManager[] { 
				new X509TrustManager() {     
					public java.security.cert.X509Certificate[] getAcceptedIssuers() { 
						return null;
					} 
					public void checkClientTrusted( 
							java.security.cert.X509Certificate[] certs, String authType) {
					} 
					public void checkServerTrusted( 
							java.security.cert.X509Certificate[] certs, String authType) {
					}
				} 
		};
		HostnameVerifier trustall = new trust();
		SSLContext sc = SSLContext.getInstance("SSL"); 
		sc.init(null, trustAllCerts, new java.security.SecureRandom());
		HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		HttpsURLConnection.setDefaultHostnameVerifier(trustall);

		String JSONString = "";
		BufferedReader BR = URLReader(serviceIP);

		StringBuilder builder = new StringBuilder();
		int counter=0;

		while ((JSONString = BR.readLine()) != null && counter<=1000) { //1000 lines chosen at random.
			builder.append(JSONString);
			counter++;    
		}
		BR.close();

		String text = builder.toString();
		return text;
	}

	private BufferedReader URLReader(String service) throws IOException {
		URL oracle = null;
		try {
			oracle = new URL(service);
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return null;
		}

		BufferedReader in = new BufferedReader(
				new InputStreamReader(oracle.openStream()));
		return in;
	}

}

