package com.beans;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

import com.classes.User;
import com.database.DatabaseInterface;
import com.utilities.Assist;
import com.utilities.Encrypt;
import com.validators.PasswordValidator;

@ManagedBean
public class ResetPasswordBean extends Assist implements Serializable {
	private static final long serialVersionUID = -2745541636395070527L;

	// Bean
	private String password;
	private String password2;
	private String[] pswComplexity = PasswordValidator.pswComplexityInfo;

	@ManagedProperty("#{param.user}")
	private String userEmail;

	@ManagedProperty("#{param.resetCode}")
	private String resetCode;

	// Bean Methods
	public String resetPassword() throws Exception {
		DatabaseInterface dbi = DatabaseInterface.getInstance();

		User user = dbi.getUser(Encrypt.decode(getParam("email")));

		if (!password.equals(password2)) {
			showError("Password missmatch.", "Passwords do not match");
			return "reset.xhtml?user=" + userEmail + "&resetCode=" + resetCode;
		}

		// Received details are correct
		// change user password
		dbi.changeUserPassword(user.getUser_email(), password);
		dbi.removeResetCode(user.getUser_email());

		showInfo("Password was changed", "Password was changed");
		persistMessage();

		return "login?faces-redirect=true";
	}

	public void isActiveReset() throws Exception {
		if (userEmail.equals(null) && userEmail.equals("")) {
			this.throwError404();
		}

		DatabaseInterface dbi = DatabaseInterface.getInstance();
		User user = dbi.getUser(Encrypt.decode(userEmail));

		if (user == null) {
			// User not found
			throwError404();
		}

		if (!user.getResetCode().equals(resetCode)) {
			// Bad reset code
			throwError404();
		}
	}

	// Getters and Setters

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setResetCode(String resetCode) {
		this.resetCode = resetCode;
	}

	public String[] getPswComplexity() {
		return pswComplexity;
	}

	public void setPassword(String input) {
		password = input;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword2(String input) {
		password2 = input;
	}

	public String getPassword2() {
		return password2;
	}
}
