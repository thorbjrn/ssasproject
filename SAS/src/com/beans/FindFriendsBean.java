package com.beans;

import java.io.Serializable;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import com.classes.User;
import com.database.DatabaseInterface;
import com.utilities.Assist;

@ManagedBean
public class FindFriendsBean extends Assist implements Serializable {
	private static final long serialVersionUID = 3724883630033792909L;

	// Security

	@ManagedProperty(value = "#{securityBean}")
	private SecurityBean auth;

	public void setAuth(SecurityBean bean) {
		this.auth = bean;
	}
	
	@ManagedProperty("#{param.search}")
	private String search;

	// Bean

	private ArrayList<User> nonFriends;
	private boolean isAnyNonFriends;
	private boolean hasSearched;
	private String displayName;

	// Methods
	public String addFriend() {
		DatabaseInterface dbi = DatabaseInterface.getInstance();

		// get friend's id
		int friendId = Integer.parseInt(getParam("userId"));

		// add friend
		dbi.addFriendToUser(auth.getUserId(), friendId);

		// display message
		User friend = dbi.getUser(friendId);
		showInfo("Friend added", friend.getDisplay_name()
				+ " successfully marked as your friend");
		persistMessage();

		return "findFriends?faces-redirect=true" + ((search == null) ? "" : ("&search=" + search));
	}

	public String doSearch() {
		DatabaseInterface dbi = DatabaseInterface.getInstance();
		return "findFriends?faces-redirect=true&search=" + displayName;
	}

	@PostConstruct
	private void loadData() {
		DatabaseInterface dbi = DatabaseInterface.getInstance();
		ArrayList<User> users = (search == null) ? dbi.getUserNonFriends(auth.getUserId()) : dbi.getUserNonFriends(auth.getUserId(), search);
		
		this.nonFriends = (users != null) ? users : new ArrayList<User>();
		
		this.isAnyNonFriends = nonFriends.size() > 0;
	}

	// Getters and Setters
	
	public void setSearch(String search) {
		this.search = search;
	}
	
	public String getSearch() {
		return search;
	}

	public ArrayList<User> getNonFriends() {
		return nonFriends;
	}

	public boolean getIsAnyNonFriends() {
		return isAnyNonFriends;
	}

	public boolean getHasSearched() {
		return hasSearched && isAnyNonFriends;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
}
