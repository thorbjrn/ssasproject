package com.beans;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

import com.classes.User;
import com.database.DatabaseInterface;
import com.utilities.Assist;

@ManagedBean
public class ProfileBean extends Assist implements Serializable {
	private static final long serialVersionUID = -4202040379673572883L;

	// Backing Beans

	@ManagedProperty(value = "#{securityBean}")
	private SecurityBean auth;

	@ManagedProperty(value = "#{romanceBean}")
	private RomanceBean romance;
	
	@ManagedProperty(value = "#{hugBean}")
	private HugBean hugs;

	public void setAuth(SecurityBean bean) {
		this.auth = bean;
	}

	public void setRomance(RomanceBean romance) {
		this.romance = romance;
	}
	
	public void setHugs(HugBean hugs) {
		this.hugs = hugs;
	}

	// Bean

	@ManagedProperty("#{param.name}")
	private String displayName;

	private User target;
	private boolean userExists;
	private boolean isMyself;
	private boolean isFriend;

	// Bean Methods

	@PostConstruct
	private void loadTarget() {
		DatabaseInterface dbi = DatabaseInterface.getInstance();
		target = dbi.getUserByDisplayName(displayName);
		romance.setTarget(target);
		hugs.setTarget(target);
	}


	// Getters and Setters

	public User getTarget() {
		return target;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getDisplayName() {
		return displayName;
	}

	public boolean getTargetExists() {
		return this.target != null;
	}

	public boolean getIsMyself() {
		User u = auth.getUser();
		return u.getDisplay_name().equals(displayName);
	}

	public boolean getIsFriend() {
		return DatabaseInterface.getInstance().areFriends(auth.getUserId(),
				target.getId());
	}
}
