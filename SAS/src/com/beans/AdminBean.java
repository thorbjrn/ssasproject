package com.beans;

import java.io.Serializable;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import com.classes.User;
import com.database.DatabaseInterface;
import com.utilities.Assist;

@ManagedBean
@RequestScoped
public class AdminBean extends Assist implements Serializable {
	private static final long serialVersionUID = 3724883630033792909L;

	// Security

	@ManagedProperty(value = "#{securityBean}")
	private SecurityBean auth;

	public void setAuth(SecurityBean bean) {
		this.auth = bean;
	}

	// Bean

	private ArrayList<User> users;
	
	@PostConstruct
	private void loadData() {
		ArrayList<User> users = DatabaseInterface.getInstance().getUsers();
		if(users != null)
			this.users = users;
		else
			this.users = new ArrayList<User>();
	}

	// Getters and Setters

	public ArrayList<User> getUsers() {
		return users;
	}
	
	public String removeUser()
	{
		int userid = Integer.parseInt(getParam("userId"));
		if(auth.getIsAdmin())
		{
			DatabaseInterface.getInstance().deleteUser(userid);
		}
		return "admin?faces-redirect=true";
	}
}
