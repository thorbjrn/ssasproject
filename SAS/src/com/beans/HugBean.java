package com.beans;

import java.io.Serializable;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import com.classes.User;
import com.database.DatabaseInterface;
import com.utilities.Assist;

@ManagedBean
public class HugBean extends Assist implements Serializable {
	private static final long serialVersionUID = 5186825943192189538L;
	
	// Security
	
	@ManagedProperty(value = "#{securityBean}")
	private SecurityBean auth;

	public void setAuth(SecurityBean bean) {
		this.auth = bean;
	}
	
	@ManagedProperty("#{param.name}")
	private String displayName;
	@ManagedProperty("#{param.source}")
	private String source;
	
	private User target; // romance candidate
	
	private ArrayList<User> uncheckedHugs;
	private boolean hasUncheckedHugs;
	
	
	// Bean Methods

	@PostConstruct
	private void loadTarget() {
		DatabaseInterface dbi = DatabaseInterface.getInstance();
		target = dbi.getUserByDisplayName(displayName);
	}
	
	public boolean getHasUncheckedHugs() {
		return DatabaseInterface.getInstance().hasUncheckedHugs(
				auth.getUserId());
	}

	
	public ArrayList<User> getUncheckedHugs() {
		
		if (uncheckedHugs == null) {
			DatabaseInterface dbi = DatabaseInterface.getInstance();
			uncheckedHugs = dbi.findUncheckedHugs(auth.getUserId());
		}
		return uncheckedHugs;
	}

	public String acceptHug(){
		DatabaseInterface dbi = DatabaseInterface.getInstance();

		String source = getParam("source");
		int senderId = Integer.parseInt(getParam("sender"));
		
		if (dbi.acceptHug(senderId, auth.getUserId())) {
			// Success
			showInfo("Hug accepted", "A hug has been accepted");
		} else {
			// DB error
			showDbError();
		}

		persistMessage();
		return source + "?faces-redirect=true";
	}
	
	public String hugPerson() {
		DatabaseInterface dbi = DatabaseInterface.getInstance();
		
		if (dbi.doesHugExists(auth.getUserId(), target.getId())){
			showInfo("A hug has already been sent to user", "A hug has already been sent");
		}else{
			if (dbi.hugPersonAsUser(auth.getUserId(), target.getId())) {
				// Success
				showInfo("Hug was sent to the user", "Hug sent!");
			}else{
				// DB error
				showDbError();
			}
		}
		
		persistMessage();
		return "profile?faces-redirect=true&name=" + displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public void setTarget(User target) {
		this.target = target;
	}
	
}
