package com.beans;

import java.io.Serializable;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import com.classes.User;
import com.database.DatabaseInterface;
import com.utilities.Assist;

@ManagedBean
@RequestScoped
public class IndexBean extends Assist implements Serializable {
	private static final long serialVersionUID = -5614414923480179687L;

	// Backing Beans

	@ManagedProperty(value = "#{securityBean}")
	private SecurityBean auth;

	@ManagedProperty(value = "#{romanceBean}")
	private RomanceBean romance;

	public void setAuth(SecurityBean bean) {
		this.auth = bean;
	}

	public void setRomance(RomanceBean romance) {
		this.romance = romance;
	}

	// Bean

	private User user;
	private ArrayList<User> friends;
	private boolean isWithFriends;

	@PostConstruct
	private void loadData() {
		User u = auth.getUser();
		this.user = (u != null) ? u : new User();
	}

	// Methods

	/**
	 * Removes user's relation with friend
	 */
	public String removeFriend() {
		DatabaseInterface dbi = DatabaseInterface.getInstance();

		// get friend's id
		int friendId = Integer.parseInt(getParam("userId"));

		// remove friend
		dbi.removeFriendFromUser(auth.getUserId(), friendId);
		// remove possible romance
		dbi.removeRomance(auth.getUserId(), friendId);

		// display message
		User friend = dbi.getUser(friendId);
		showInfo("Friend removed", friend.getDisplay_name()
				+ " successfully removed from friends list");
		persistMessage();

		return "index?faces-redirect=true";
	}

	/**
	 * Reloads user's friends from DB
	 */
	private void reloadFriends() {
		ArrayList<User> f = DatabaseInterface.getInstance().getUserFriends(
				auth.getUserId());
		this.friends = (f != null) ? f : new ArrayList<User>();

		this.isWithFriends = this.friends.size() > 0;
	}

	// Setters/Getter

	public User getUser() {
		return this.user;
	}

	public ArrayList<User> getFriends() {
		if (friends == null)
			reloadFriends();
		return friends;
	}

	public boolean getIsWithFriends() {
		if (friends == null)
			reloadFriends();
		return isWithFriends;
	}

}
