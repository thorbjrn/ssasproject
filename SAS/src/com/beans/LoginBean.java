package com.beans;

import java.io.IOException;
import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

@ManagedBean
@ViewScoped
public class LoginBean implements Serializable {
	private static final long serialVersionUID = -129403246754738110L;

	// Security

	@ManagedProperty(value = "#{securityBean}")
	private SecurityBean auth;

	public void setAuth(SecurityBean bean) {
		this.auth = bean;
	}

	// Bean

	private String username;
	private String password;

	// Getters and Setters

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	// Bean Methods

	public String login() throws IOException {
		FacesContext fc = FacesContext.getCurrentInstance();

		if (auth.login(username, password) == true) {
			ExternalContext ec = fc.getExternalContext();
			if(auth.getUser().getRole_code().equals("ADMIN"))
				ec.redirect("admin.xhtml");
			else
				ec.redirect("index.xhtml");
		}

		return null;
	}
}
