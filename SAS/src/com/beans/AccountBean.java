package com.beans;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import com.classes.User;
import com.database.DatabaseInterface;
import com.utilities.Assist;
import com.utilities.Auth;
import com.utilities.Encrypt;
import com.utilities.Hasher;
import com.utilities.Mail;
import com.validators.Validate;

@ManagedBean
public class AccountBean extends Assist implements Serializable {
	private static final long serialVersionUID = 3083673533611718219L;
	
	// Security

	@ManagedProperty(value = "#{securityBean}")
	private SecurityBean auth;
	
	private String userEmailAddress;
	

	public void setAuth(SecurityBean bean) {
		this.auth = bean;
	}

	// Bean

	@ManagedProperty("#{param.user}")
	private String userEmail;

	@ManagedProperty("#{param.code}")
	private String activationCode;
	
	// Bean Methods
	public void showCreatedPage() {
		String show = auth.getSession().get("show_account_created");
		
		if (show == null || !show.equals("show")) {
			// throw Access Denied
			throwError404();
		}
	}
	
	public void activateAccount() throws Exception {
		DatabaseInterface dbi = DatabaseInterface.getInstance();
		User user = dbi.getUser(Encrypt.decode(userEmail));
		
		if (user == null) {
			// User not found
			throwError404();
		}
		
		if (user.getIs_active().equals("Y")) {
			// User already active
			throwError404();
		}
		
		if (!activationCode.equals(user.getActivation_code())) {
			// Activation code mismatch
			throwError404();
		}
		
		// Received details are correct
		// Activate User
		dbi.activateAccount(user.getUser_email());
	}
	
	public String resetPassword(){ //used for sending reset mail.
		if (userEmailAddress.trim().length() > 0 && Validate.email(userEmailAddress)) {
			// valid email submitted
			DatabaseInterface dbi = DatabaseInterface.getInstance();
			User user = dbi.getUser(userEmailAddress);
			
			if (user != null && user.getIs_deleted() == null) {
				
				if (user.getIs_active().equals("N")) {
					// account is not activated
					showError("Account inactive.", "The account for provided email was not activated yet.");
					return null;
				}
				
				// Create reset request
				String code = Auth.hash_password(Hasher.toSHA256(user.getUser_password()));
				dbi.setResetCode(userEmailAddress, code);

				// Notify User
				user.setResetCode(code);
				try {
					Mail.sendPasswordResetEmail(user);
				} catch (Exception e) {
					System.out.println("Failed in sending Password Reset email");
					System.out.println(e.getMessage());
					
					showError("Password reset error", "System encountered a problem when reseting your password. Please try again.");
					return null;
				}
				
				// Success
				showInfo("Password reset request sent", "We sent a mail to you, regarding getting access to your page again. Please follow the instructions in the mail.");
				persistMessage();
				return "login?faces-redirect=true";
			}
			
			// Account not found or Banned.
			showError("Password reset request failed", "Invalid email submitted");
			return null;
		}
		
		// Invalid email.
		showError("Password reset email error", "Invalid email submitted");
		return null;
	}
	
	// Getters and Setter
	
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	
	public void setActivationCode(String activationCode) {
		this.activationCode = activationCode;
	}	
	
	public void setUserEmailAddress(String input){
		userEmailAddress = input;
	}
	
	public String getUserEmailAddress(){
		return userEmailAddress;
	}
	
}
