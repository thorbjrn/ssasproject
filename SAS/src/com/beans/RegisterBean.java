package com.beans;

import java.io.IOException;
import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import com.classes.User;
import com.database.DatabaseInterface;
import com.utilities.Assist;
import com.utilities.Auth;
import com.utilities.Mail;
import com.validators.DisplayNameValidator;
import com.validators.PasswordValidator;
import com.validators.UsernameValidator;

@ManagedBean
@RequestScoped
public class RegisterBean extends Assist implements Serializable {
	private static final long serialVersionUID = 2246381563643662456L;

	// Security

	@ManagedProperty(value = "#{securityBean}")
	private SecurityBean auth;

	public void setAuth(SecurityBean bean) {
		this.auth = bean;
	}

	// Bean

	private String displayName;
	private String username;
	private String password;
	private String repeatPassword;

	private String prefix;
	private String firstName;
	private String lastName;
	private String address;

	private String[] pswComplexity = PasswordValidator.pswComplexityInfo;
	private String[] usrComplexity = UsernameValidator.usrComplexityInfo;
	private String[] dnComplexity = DisplayNameValidator.dnComplexityInfo;
	private String[] prefixes = User.prefixes;

	// Bean Methods

	public String register() throws IOException {
		// Note: inputs have already passed their validation

		FacesContext fc = FacesContext.getCurrentInstance();
		DatabaseInterface dbi = DatabaseInterface.getInstance();

		if (!password.equals(repeatPassword)) {
			showError("Password missmatch.", "Passwords do not match");
			return null;
		}

		if (dbi.userExists(username)) {
			showError("User exists.", "User with given username already exists");
			return null;
		}

		if (dbi.displayNameExists(displayName)) {
			showError("Display Name exists.", "User with given Display Name already exists");
			return null;
		}

		if (dbi.createUser(displayName, username,
				Auth.hash_password(password))) {
			// Registration was successful

			// Adding profile details to user
			User u = new User();
			u.setDisplay_name(displayName);
			u.setUser_email(username);
			u.setPrefix(prefix);
			u.setFirst_name(firstName);
			u.setLast_name(lastName);
			u.setAddress(address);
			dbi.updateProfile(u);
			
			// Account created
			// Send activation email
			
			User user = dbi.getUser(username);
			
			try {
				Mail.sendActivationEmail(user);
			} catch (Exception e) {
				System.out.println("Fail in RegisterBean - sendActivationEmail");
				System.out.println(e.getMessage());
			}
			
			auth.getSession().put("show_account_created", "show");
			auth.getSession().put("account_name", user.getUser_email());
			fc.getExternalContext().redirect("created.xhtml");
		}

		// Registration failed
		return null;
	}

	// Getters and Setters

	public String[] getPswComplexity() {
		return pswComplexity;
	}

	public String[] getUsrComplexity() {
		return usrComplexity;
	}

	public String[] getDnComplexity() {
		return dnComplexity;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username.trim();
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password.trim();
	}

	public String getRepeatPassword() {
		return repeatPassword;
	}

	public void setRepeatPassword(String repeatPassword) {
		this.repeatPassword = repeatPassword.trim();
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String[] getPrefixes() {
		return prefixes;
	}
}
