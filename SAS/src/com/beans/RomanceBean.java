package com.beans;

import java.io.Serializable;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

import com.classes.User;
import com.database.DatabaseInterface;
import com.utilities.Assist;

@ManagedBean
public class RomanceBean extends Assist implements Serializable {
	private static final long serialVersionUID = -1166068735619550810L;

	// Security

	@ManagedProperty(value = "#{securityBean}")
	private SecurityBean auth;

	public void setAuth(SecurityBean bean) {
		this.auth = bean;
	}

	// Bean

	@ManagedProperty("#{param.name}")
	private String displayName;
	@ManagedProperty("#{param.source}")
	private String source;

	private User target; // romance candidate
	private User romance; // User's romance (request or active)
	private ArrayList<User> pendingRequests;
	private boolean isInRomance;
	private boolean hasRomance;
	private boolean hasRomanceRequest;
	private boolean hasPendingRequest;
	private boolean hasDeclineRequest;

	// Bean Methods

	@PostConstruct
	private void loadTarget() {
		DatabaseInterface dbi = DatabaseInterface.getInstance();
		target = dbi.getUserByDisplayName(displayName);
	}

	/**
	 * Sends Romance request to target
	 */
	public String requestRomance() {
		DatabaseInterface dbi = DatabaseInterface.getInstance();

		if (dbi.requestRomance(auth.getUserId(), target.getId())) {
			// Success
			showInfo("Romance request created", "Romance request sent");
		} else {
			// DB error
			showDbError();
		}

		persistMessage();
		return source + "?faces-redirect=true&name=" + displayName;
	}

	/**
	 * Declines pending romance request
	 */
	public String declineRequest() {
		DatabaseInterface dbi = DatabaseInterface.getInstance();

		String source = getParam("source");
		int senderId = Integer.parseInt(getParam("sender"));

		if (dbi.declineRequest(senderId, auth.getUserId())) {
			// Success
			showInfo("Romance declined", "Romance request has been declined");
		} else {
			// DB error
			showDbError();
		}

		persistMessage();
		return source + "?faces-redirect=true";
	}

	/**
	 * Accept pending romance request
	 */
	public String acceptRequest() {
		DatabaseInterface dbi = DatabaseInterface.getInstance();

		String source = getParam("source");
		int senderId = Integer.parseInt(getParam("sender"));

		if (dbi.acceptRequest(senderId, auth.getUserId())) {
			// Success
			showInfo("Romance accepted", "Romance request has been accepted");
		} else {
			// DB error
			showDbError();
		}

		persistMessage();
		return source + "?faces-redirect=true";
	}

	/**
	 * Set Decline notice to read
	 */
	public String confirmDeclineNotice() {
		DatabaseInterface dbi = DatabaseInterface.getInstance();

		String source = getParam("source");

		if (!dbi.readDeclineNotice(auth.getUserId())) {
			// DB error
			showDbError();
			persistMessage();
		}

		return source + "?faces-redirect=true";
	}

	// Getters and Setters

	// Getter and Setters

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public void setTarget(User target) {
		this.target = target;
	}

	public User getRomance() {
		if (romance == null) {
			DatabaseInterface dbi = DatabaseInterface.getInstance();

			if (getHasRomance()) // User's romance
				romance = dbi.findRomance(auth.getUserId());

			if (getHasRomanceRequest()) // Pending romance
				romance = dbi.findPendingRomance(auth.getUserId());
		}
		return romance;
	}

	public ArrayList<User> getPendingRequests() {
		if (pendingRequests == null) {
			DatabaseInterface dbi = DatabaseInterface.getInstance();
			pendingRequests = dbi.findPendingRequests(auth.getUserId());
		}
		return pendingRequests;
	}

	public boolean getIsInRomance() {
		DatabaseInterface dbi = DatabaseInterface.getInstance();
		// check if user already has romance
		boolean userRomance = getHasRomance();
		// check if user has pending romance request
		boolean hasRomancesPending = getHasRomanceRequest();
		// check if target already has a romance
		boolean targetRomance = dbi.hasRomance(target.getId());

		return (userRomance || hasRomancesPending || targetRomance);
	}

	public boolean getHasRomance() {
		return DatabaseInterface.getInstance().hasRomance(auth.getUserId());
	}

	public boolean getHasRomanceRequest() {
		return DatabaseInterface.getInstance().hasPendingRomance(
				auth.getUserId());
	}

	public boolean getHasPendingRequest() {
		return DatabaseInterface.getInstance().hasPendingRequest(
				auth.getUserId());
	}

	public boolean getHasDeclineRequest() {
		return DatabaseInterface.getInstance().hasDeclineRequest(
				auth.getUserId());
	}
}
