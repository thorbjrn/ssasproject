package com.beans;

import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.classes.User;
import com.database.DatabaseInterface;
import com.utilities.Assist;
import com.utilities.Auth;

@ManagedBean
@SessionScoped
public class SecurityBean extends Assist implements Serializable {
	private static final long serialVersionUID = 6028699363234474450L;

	// Bean
	
	private String userEmail;
	private int userId;
	private HashMap<String, String> session;

	private boolean isLoggedIn;
	private boolean isAdmin; // user is ADMIN

	public SecurityBean() {
		session = new HashMap<String, String>();
	}

	// Bean Methods

	/**
	 * Checks if User is logged in as ADMIN
	 */
	public void isAdmin() throws Exception {
		loggedIn(); // check if logged in

		if (!getIsAdmin()) {
			throwError404();
		}
	}
	
	/**
	 * Forbid not logged user
	 */
	public void loggedIn() throws Exception {
		if (!getIsLoggedIn()) {
			FacesContext fc = FacesContext.getCurrentInstance();
			ExternalContext efc = fc.getExternalContext();
			efc.redirect("login.xhtml");
			fc.responseComplete();
		}
	}
	
	/**
	 * Forbid logged in users
	 */
	public void dontAllowLogged() throws IOException {
		if (getIsLoggedIn()) {
			// User found. Redirect to index page
			FacesContext fc = FacesContext.getCurrentInstance();
			ExternalContext efc = fc.getExternalContext();
			efc.redirect("index.xhtml");
			fc.responseComplete();
		}
	}

	/**
	 * Attempting to login user with given details
	 */
	public boolean login(String username, String password) {
		if (password == null) {
			// password missing
			showError("Missing password", "Incorrect Username or Password.");
			return false;
		}

		// check for User with given email
		User u = DatabaseInterface.getInstance().getUser(username);
		// create hashed password
		password = Auth.hash_password(password);
		
		// checks if password is right and if account is active and not locked
		if (u != null) {

			// Check if account is active
			if (u.getIs_active().equals("N")) {
				showError("Account inactive", "Your account is inactive.");
				showError("Account inactive",
						"Please check your email and activate the account.");

				return false;
			}

			// Check lockout status
			if (u.getIs_locked().equals("Y")) {
				boolean locked = this.checkLockout(u);
				if (locked) {
					long left = 5 - this.minutesPassed(u.getLast_login());

					showError("Account locked", "Account has been locked.");
					showError("Account locked", "" + left
							+ " minute(s) remaining.");

					return false;
				}
			}
			if (u != null && password.equals(u.getUser_password())) {
				this.complete_login(u);
				return true;
			}

			// failed to login
			this.completeFailedLogin(u);
		}

		showError("Incorrect credentials", "Incorrect Username or Password.");

		return false;
	}

	/**
	 * Attempting to logout current user
	 */
	public void logout() throws IOException {
		userEmail = null;
		userId = 0;

		FacesContext fc = FacesContext.getCurrentInstance();
		ExternalContext efc = fc.getExternalContext();
		HttpSession httpSession = (HttpSession) efc.getSession(false);
		
		httpSession.invalidate(); // destroy current session
		efc.redirect("login.xhtml");
	}

	/**
	 * Check if account's lockout has ended
	 */
	private boolean checkLockout(User u) {
		long diffMinutes = this.minutesPassed(u.getLast_login());
		if (diffMinutes >= 5) {
			DatabaseInterface.getInstance().unlockUser(u.getId());
			return false;
		}

		return true;
	}

	/**
	 * Completes successful login attempt
	 */
	private void complete_login(User u) {
		// get IP address
		String ipAddress = this.getIpAddress();
		// update details on DB
		DatabaseInterface.getInstance().completeLogin(u.getUser_email(),
				ipAddress);

		// store user details
		userEmail = u.getUser_email();
		userId = u.getId();
	}

	/**
	 * Completes unsuccessful login attempt
	 */
	private void completeFailedLogin(User u) {
		String ipAddress = this.getIpAddress();
		DatabaseInterface.getInstance().completeFailedLogin(u.getId(),
				ipAddress);
	}

	/**
	 * Get user's IP address
	 */
	private String getIpAddress() {
		FacesContext fc = FacesContext.getCurrentInstance();
		ExternalContext efc = fc.getExternalContext();
		HttpServletRequest request = (HttpServletRequest) efc.getRequest();

		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress != null && !ipAddress.isEmpty()) {
			ipAddress = ipAddress.split("\\s*,\\s*", 2)[0];
		} else {
			ipAddress = request.getRemoteAddr();
		}
		return ipAddress;
	}

	/**
	 * Returns a number of minutes passed until now.
	 */
	private long minutesPassed(String time) {
		try {
			// Date values
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date lastLogin = dateFormat.parse(time);
			Date now = new Date();

			// Check if lockout has passed
			long diff = now.getTime() - lastLogin.getTime();
			long diffMinutes = diff / (60 * 1000) % 60;
			return diffMinutes;
		} catch (ParseException e) {
			System.out.println("Fail in SecurityBean - minutesPassed");
			System.out.println(e.getMessage());
		}
		return -1;
	}
	
	// Getters and Setter
	
	public HashMap<String, String> getSession() {
		return session;
	}
	
	public String getUserEmail() {
		return userEmail;
	}

	public int getUserId() {
		return userId;
	}

	public User getUser() {
		return DatabaseInterface.getInstance().getUser(userId);
	}

	public boolean getIsAdmin() {
		return (getIsLoggedIn() && getUser().getRole_code().equals("ADMIN"));
	}

	public boolean getIsLoggedIn() {
		return (userEmail != null && userId != 0);
	}

}
