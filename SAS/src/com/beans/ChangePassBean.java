package com.beans;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import com.database.DatabaseInterface;
import com.utilities.Auth;

@ManagedBean
@SessionScoped
public class ChangePassBean implements Serializable {

	private static final long serialVersionUID = 8305937387350840382L;

	private String oldPassword;
	private String newPassword;
	private String repeatNewPassword;
	
	// Security

	@ManagedProperty(value = "#{securityBean}")
	private SecurityBean auth;
	
	public void setAuth(SecurityBean bean) {
		this.auth = bean;
	}
	
	
	public String update() {		
		
		FacesContext fc = FacesContext.getCurrentInstance();
		
		if (auth.login(auth.getUser().getUser_email(), oldPassword) == false) {
			return null;
		}		

		if (!newPassword.equals(repeatNewPassword)) {
			fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
					"Password missmatch.", "Passwords do not match"));
			return null;
		}
		
		DatabaseInterface.getInstance().changeUserPassword(auth.getUser().getUser_email(), newPassword);
		FacesContext.getCurrentInstance().addMessage(
				null,
				new FacesMessage(FacesMessage.SEVERITY_INFO,
						"Password changed.", "Password has been changed"));
		
			
		return null;
	}
	
	public String getOldPassword() {
		return oldPassword;
	}
	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}
	public String getNewPassword() {
		return newPassword;
	}
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	public String getRepeatNewPassword() {
		return repeatNewPassword;
	}
	public void setRepeatNewPassword(String repeatNewPassword) {
		this.repeatNewPassword = repeatNewPassword;
	}
}
