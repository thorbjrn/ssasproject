package com.beans;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

import com.classes.User;
import com.database.DatabaseInterface;

@ManagedBean
@RequestScoped
public class SettingsBean implements Serializable {
	private static final long serialVersionUID = 5087973857078103464L;

	// Security

	@ManagedProperty(value = "#{securityBean}")
	private SecurityBean auth;

	public void setAuth(SecurityBean bean) {
		this.auth = bean;
	}

	// Bean

	private User user;
	private String[] prefixes = User.prefixes;

	@PostConstruct
	private void loadUser() {
		this.user = auth.getUser();
	}

	public String update() {
		if (!auth.getUser().getDisplay_name()
				.equals(this.user.getDisplay_name())) {
			// Display name changed
			if (DatabaseInterface.getInstance().displayNameExists(
					this.user.getDisplay_name())) {
				FacesContext.getCurrentInstance().addMessage(
						null,
						new FacesMessage(FacesMessage.SEVERITY_ERROR,
								"Display Name exists.",
								"User with given Display Name already exists"));
				return null;
			}
		}

		DatabaseInterface.getInstance().updateProfile(this.user);
		FacesContext.getCurrentInstance().addMessage(
				null,
				new FacesMessage(FacesMessage.SEVERITY_INFO,
						"Account updated.", "Account details updated"));
		
		return null;
	}

	// Setters/Getter

	public User getUser() {
		return user;
	}

	public String[] getPrefixes() {
		return prefixes;
	}

	// Value Changed Listener

	public void prefixChanged(ValueChangeEvent e) {
		this.user.setPrefix(e.getNewValue().toString());
	}

	public void firstNameChanged(ValueChangeEvent e) {
		this.user.setFirst_name(e.getNewValue().toString());
	}

	public void lastNameChanged(ValueChangeEvent e) {
		this.user.setLast_name(e.getNewValue().toString());
	}

	public void addressChanged(ValueChangeEvent e) {
		this.user.setAddress(e.getNewValue().toString());
	}

	public void hobbiesChanged(ValueChangeEvent e) {
		this.user.setHobbies(e.getNewValue().toString());
	}

	public void aboutChanged(ValueChangeEvent e) {
		this.user.setAbout(e.getNewValue().toString());
	}

	public void shareNameChanged(ValueChangeEvent e) {
		this.user.setShare_name((Boolean) e.getNewValue());
	}

	public void shareAddressChanged(ValueChangeEvent e) {
		this.user.setShare_address((Boolean) e.getNewValue());
	}

	public void display_nameChanged(ValueChangeEvent e) {
		this.user.setDisplay_name(e.getNewValue().toString());
	}

	// private boolean shareName = false;
	// private boolean shareAddress = false;

}
