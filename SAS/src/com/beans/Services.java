package com.beans;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.utilities.Scraper;

@SuppressWarnings("serial")
public class Services extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		String url = req.getParameter("url");
		
		if(!(url.contains("192.237.215.145") || url.contains("166.78.184.170")))
			url = "https://192.237.211.24/SAS/PageNotFound.xhtml";
		String json = "";

		Scraper SC = new Scraper();
		try {
			json = SC.ScrapeURL(url);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		resp.getWriter().print(json);
	}
}
