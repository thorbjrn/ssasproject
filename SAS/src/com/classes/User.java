package com.classes;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;

public class User implements Serializable {
	private static final long serialVersionUID = -3679964194849578519L;

	private int id;
	private String role_code;
	private String user_password;
	private String user_email;
	private String display_name;
	private String prefix;
	private String first_name;
	private String last_name;
	private String address;
	private String hobbies;
	private String about;
	private String activation_code;
	private String is_active;
	private String inactive_after;
	private int count_login;
	private String last_login;
	private String last_login_ip;
	private String is_locked;
	private int try_until;
	private String created;
	private int created_by;
	private String modified;
	private int modified_by;
	private String is_deleted;
	private boolean share_name;
	private boolean share_address;
	private String resetCode;

	public static String[] prefixes = new String[] { "Mr.", "Mrs.", "Miss" };
	public static String[] roles = new String[] { "USER", "ADMIN" };

	public User() {

	}

	/**
	 * Create user from DB result
	 * 
	 * @param result
	 * @throws SQLException
	 */
	public User(ResultSet result) throws SQLException {
		this.id = result.getInt("id");
		this.role_code = result.getString("role_code");
		this.user_password = result.getString("user_password");
		this.user_email = result.getString("user_email");
		this.display_name = result.getString("display_name");
		this.prefix = result.getString("prefix");
		this.first_name = result.getString("first_name");
		this.last_name = result.getString("last_name");
		this.address = result.getString("address");
		this.hobbies = result.getString("hobbies");
		this.about = result.getString("about");
		this.activation_code = result.getString("activation_code");
		this.is_active = result.getString("is_active");
		this.inactive_after = result.getString("inactive_after");
		this.count_login = result.getInt("count_login");
		this.last_login = result.getString("last_login");
		this.last_login_ip = result.getString("last_login_ip");
		this.is_locked = result.getString("is_locked");
		this.try_until = result.getInt("try_until");
		this.created = result.getString("created");
		this.created_by = result.getInt("created_by");
		this.modified = result.getString("modified");
		this.modified_by = result.getInt("modified_by");
		this.is_deleted = result.getString("is_deleted");
		this.share_name = result.getBoolean("share_name");
		this.share_address = result.getBoolean("share_address");
		this.resetCode = result.getString("reset_code");
	}

	/**
	 * Create user with passed data from DB columns
	 * 
	 * @param result
	 * @param columns
	 * @throws SQLException
	 */
	public User(ResultSet result, String[] columns) throws SQLException {
		for (String c : columns) {
			if (c.equals("id")) {
				id = result.getInt("id");
			} else if (c.equals("role_code")) {
				role_code = result.getString("role_code");
			} else if (c.equals("user_password")) {
				user_password = result.getString("user_password");
			} else if (c.equals("user_email")) {
				user_email = result.getString("user_email");
			} else if (c.equals("display_name")) {
				display_name = result.getString("display_name");
			} else if (c.equals("prefix")) {
				prefix = result.getString("prefix");
			} else if (c.equals("first_name")) {
				first_name = result.getString("first_name");
			} else if (c.equals("last_name")) {
				last_name = result.getString("last_name");
			} else if (c.equals("address")) {
				address = result.getString("address");
			} else if (c.equals("hobbies")) {
				hobbies = result.getString("hobbies");
			} else if (c.equals("about")) {
				about = result.getString("about");
			} else if (c.equals("activation_code")) {
				activation_code = result.getString("activation_code");
			} else if (c.equals("is_active")) {
				is_active = result.getString("is_active");
			} else if (c.equals("inactive_after")) {
				inactive_after = result.getString("inactive_after");
			} else if (c.equals("count_login")) {
				count_login = result.getInt("count_login");
			} else if (c.equals("last_login")) {
				last_login = result.getString("last_login");
			} else if (c.equals("last_login_ip")) {
				last_login_ip = result.getString("last_login_ip");
			} else if (c.equals("is_locked")) {
				is_locked = result.getString("is_locked");
			} else if (c.equals("try_until")) {
				try_until = result.getInt("try_until");
			} else if (c.equals("created")) {
				created = result.getString("created");
			} else if (c.equals("created_by")) {
				created_by = result.getInt("created_by");
			} else if (c.equals("modified")) {
				modified = result.getString("modified");
			} else if (c.equals("modified_by")) {
				modified_by = result.getInt("modified_by");
			} else if (c.equals("is_deleted")) {
				is_deleted = result.getString("is_deleted");
			} else if (c.equals("share_name")) {
				share_name = result.getBoolean("share_name");
			} else if (c.equals("share_address")) {
				share_address = result.getBoolean("share_address");
			} else if (c.equals("reset_code")) {
				resetCode = result.getString("reset_code");
			}
		}
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getRole_code() {
		return role_code;
	}

	public void setRole_code(String role_code) {
		this.role_code = role_code;
	}

	public String getUser_password() {
		return user_password;
	}

	public void setUser_password(String user_password) {
		this.user_password = user_password;
	}

	public String getUser_email() {
		return user_email;
	}

	public void setUser_email(String user_email) {
		this.user_email = user_email;
	}

	public String getDisplay_name() {
		return display_name;
	}

	public void setDisplay_name(String display_name) {
		this.display_name = display_name;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getHobbies() {
		return hobbies;
	}

	public void setHobbies(String hobbies) {
		this.hobbies = hobbies;
	}

	public String getAbout() {
		return about;
	}

	public void setAbout(String about) {
		this.about = about;
	}

	public String getActivation_code() {
		return activation_code;
	}

	public void setActivation_code(String activation_code) {
		this.activation_code = activation_code;
	}

	public String getIs_active() {
		return is_active;
	}

	public void setIs_active(String is_active) {
		this.is_active = is_active;
	}

	public String getInactive_after() {
		return inactive_after;
	}

	public void setInactive_after(String inactive_after) {
		this.inactive_after = inactive_after;
	}

	public int getCount_login() {
		return count_login;
	}

	public void setCount_login(int count_login) {
		this.count_login = count_login;
	}

	public String getLast_login() {
		return last_login;
	}

	public void setLast_login(String last_login) {
		this.last_login = last_login;
	}

	public String getLast_login_ip() {
		return last_login_ip;
	}

	public void setLast_login_ip(String last_login_ip) {
		this.last_login_ip = last_login_ip;
	}

	public String getIs_locked() {
		return is_locked;
	}

	public void setIs_locked(String is_locked) {
		this.is_locked = is_locked;
	}

	public int getTry_until() {
		return try_until;
	}

	public void setTry_until(int try_until) {
		this.try_until = try_until;
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public int getCreated_by() {
		return created_by;
	}

	public void setCreated_by(int created_by) {
		this.created_by = created_by;
	}

	public String getModified() {
		return modified;
	}

	public void setModified(String modified) {
		this.modified = modified;
	}

	public int getModified_by() {
		return modified_by;
	}

	public void setModified_by(int modified_by) {
		this.modified_by = modified_by;
	}

	public String getIs_deleted() {
		return is_deleted;
	}

	public void setIs_deleted(String is_deleted) {
		this.is_deleted = is_deleted;
	}
	
	public boolean getShare_name() {
		return share_name;
	}

	public void setShare_name(boolean share_name) {
		this.share_name = share_name;
	}

	public boolean getShare_address() {
		return share_address;
	}

	public void setShare_address(boolean share_address) {
		this.share_address = share_address;
	}

	public void setResetCode(String input_resetCode){
		this.resetCode = input_resetCode;
	}
	
	public String getResetCode() {
		return this.resetCode;
	}
	
	@Override
	public String toString() {
		return "User [id=" + id + ", role_code=" + role_code
				+ ", user_password=" + user_password + ", user_email="
				+ user_email + ", display_name=" + display_name + ", prefix="
				+ prefix + ", first_name=" + first_name + ", last_name="
				+ last_name + ", address=" + address + ", hobbies=" + hobbies
				+ ", about=" + about + ", activation_code=" + activation_code
				+ ", is_active=" + is_active + ", inactive_after="
				+ inactive_after + ", count_login=" + count_login
				+ ", last_login=" + last_login + ", last_login_ip="
				+ last_login_ip + ", is_locked=" + is_locked + ", try_until="
				+ try_until + ", created=" + created + ", created_by="
				+ created_by + ", modified=" + modified + ", modified_by="
				+ modified_by + ", is_deleted=" + is_deleted + ", share_name="
				+ share_name + ", share_address=" + share_address
				+ ", resetCode=" + resetCode + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((about == null) ? 0 : about.hashCode());
		result = prime * result
				+ ((activation_code == null) ? 0 : activation_code.hashCode());
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + count_login;
		result = prime * result + ((created == null) ? 0 : created.hashCode());
		result = prime * result + created_by;
		result = prime * result
				+ ((display_name == null) ? 0 : display_name.hashCode());
		result = prime * result
				+ ((first_name == null) ? 0 : first_name.hashCode());
		result = prime * result + ((hobbies == null) ? 0 : hobbies.hashCode());
		result = prime * result + id;
		result = prime * result
				+ ((inactive_after == null) ? 0 : inactive_after.hashCode());
		result = prime * result
				+ ((is_active == null) ? 0 : is_active.hashCode());
		result = prime * result
				+ ((is_deleted == null) ? 0 : is_deleted.hashCode());
		result = prime * result
				+ ((is_locked == null) ? 0 : is_locked.hashCode());
		result = prime * result
				+ ((last_login == null) ? 0 : last_login.hashCode());
		result = prime * result
				+ ((last_login_ip == null) ? 0 : last_login_ip.hashCode());
		result = prime * result
				+ ((last_name == null) ? 0 : last_name.hashCode());
		result = prime * result
				+ ((modified == null) ? 0 : modified.hashCode());
		result = prime * result + modified_by;
		result = prime * result + ((prefix == null) ? 0 : prefix.hashCode());
		result = prime * result
				+ ((role_code == null) ? 0 : role_code.hashCode());
		result = prime * result + (share_address ? 1231 : 1237);
		result = prime * result + (share_name ? 1231 : 1237);
		result = prime * result + try_until;
		result = prime * result
				+ ((user_email == null) ? 0 : user_email.hashCode());
		result = prime * result
				+ ((user_password == null) ? 0 : user_password.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (about == null) {
			if (other.about != null)
				return false;
		} else if (!about.equals(other.about))
			return false;
		if (activation_code == null) {
			if (other.activation_code != null)
				return false;
		} else if (!activation_code.equals(other.activation_code))
			return false;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (count_login != other.count_login)
			return false;
		if (created == null) {
			if (other.created != null)
				return false;
		} else if (!created.equals(other.created))
			return false;
		if (created_by != other.created_by)
			return false;
		if (display_name == null) {
			if (other.display_name != null)
				return false;
		} else if (!display_name.equals(other.display_name))
			return false;
		if (first_name == null) {
			if (other.first_name != null)
				return false;
		} else if (!first_name.equals(other.first_name))
			return false;
		if (hobbies == null) {
			if (other.hobbies != null)
				return false;
		} else if (!hobbies.equals(other.hobbies))
			return false;
		if (id != other.id)
			return false;
		if (inactive_after == null) {
			if (other.inactive_after != null)
				return false;
		} else if (!inactive_after.equals(other.inactive_after))
			return false;
		if (is_active == null) {
			if (other.is_active != null)
				return false;
		} else if (!is_active.equals(other.is_active))
			return false;
		if (is_deleted == null) {
			if (other.is_deleted != null)
				return false;
		} else if (!is_deleted.equals(other.is_deleted))
			return false;
		if (is_locked == null) {
			if (other.is_locked != null)
				return false;
		} else if (!is_locked.equals(other.is_locked))
			return false;
		if (last_login == null) {
			if (other.last_login != null)
				return false;
		} else if (!last_login.equals(other.last_login))
			return false;
		if (last_login_ip == null) {
			if (other.last_login_ip != null)
				return false;
		} else if (!last_login_ip.equals(other.last_login_ip))
			return false;
		if (last_name == null) {
			if (other.last_name != null)
				return false;
		} else if (!last_name.equals(other.last_name))
			return false;
		if (modified == null) {
			if (other.modified != null)
				return false;
		} else if (!modified.equals(other.modified))
			return false;
		if (modified_by != other.modified_by)
			return false;
		if (prefix == null) {
			if (other.prefix != null)
				return false;
		} else if (!prefix.equals(other.prefix))
			return false;
		if (role_code == null) {
			if (other.role_code != null)
				return false;
		} else if (!role_code.equals(other.role_code))
			return false;
		if (share_address != other.share_address)
			return false;
		if (share_name != other.share_name)
			return false;
		if (try_until != other.try_until)
			return false;
		if (user_email == null) {
			if (other.user_email != null)
				return false;
		} else if (!user_email.equals(other.user_email))
			return false;
		if (user_password == null) {
			if (other.user_password != null)
				return false;
		} else if (!user_password.equals(other.user_password))
			return false;
		return true;
	}
	
}
