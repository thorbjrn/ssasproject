package com.classes;

import java.io.Serializable;

public class Tables implements Serializable {
	private static final long serialVersionUID = -724141008804816408L;

	public static final String USERS = "users";
	public static final String USER_RELATION = "user_relation";
	public static final String SESSIONS = "sessions";
	public static final String HUGS = "hugs";
	public static final String ROMANCE_REQUEST = "romance_request";
}
