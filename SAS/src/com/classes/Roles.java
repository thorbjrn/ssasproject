package com.classes;

import java.io.Serializable;

public class Roles implements Serializable {
	private static final long serialVersionUID = -3366835591475064596L;
	
	public final static String USER = "USER";
	public final static String ADMIN = "ADMIN";
}
