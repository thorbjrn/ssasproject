package com.classes;

import java.net.URL;
import java.util.ArrayList;

public class ForeignUser { //This class is for constructing and handling users from the other teams platforms.
	
	private String nick;
	private URL link;
	private String Address;
	private String hobbies;
	private ArrayList<String>[] relations;
	private ArrayList<URL>[] relationLinks;
	private ArrayList<String>[] relationType;
	
	public ForeignUser(){
		//TODO make a good constructor for this, instead of using setters?			
	}
	
	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public URL getLink() {
		return link;
	}

	public void setLink(URL link) {
		this.link = link;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}

	public String getHobbies() {
		return hobbies;
	}

	public void setHobbies(String hobbies) {
		this.hobbies = hobbies;
	}

	public ArrayList<String>[] getRelations() {
		return relations;
	}

	public void setRelations(ArrayList<String>[] relations) {
		this.relations = relations;
	}

	public ArrayList<URL>[] getRelationLinks() {
		return relationLinks;
	}

	public void setRelationLinks(ArrayList<URL>[] relationLinks) {
		this.relationLinks = relationLinks;
	}

	public ArrayList<String>[] getRelationType() {
		return relationType;
	}

	public void setRelationType(ArrayList<String>[] relationType) {
		this.relationType = relationType;
	}
}
