package com.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.classes.User;

public class DatabaseInterface {
	// Database Connection Detail

	// server
	private String sqluser = "webapp";
	private String sqlpw = "E19928hu";
	private String databaseName = "SAS";

	private static DatabaseInterface instance;
	private UserMapper user_mapper; // handle single user
	private UsersMapper users_mapper; // handle multiple users
	private RomanceMapper romance_mapper;
	private UserRelationMapper user_relation_mapper;

	public DatabaseInterface() {
		user_mapper = new UserMapper();
		users_mapper = new UsersMapper();
		romance_mapper = new RomanceMapper();
		user_relation_mapper = new UserRelationMapper();
	}

	public static DatabaseInterface getInstance() { // singleton
		if (instance == null) {
			instance = new DatabaseInterface();
		}
		return instance;
	}

	private Connection openDataBase() {
		Connection con = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/"
					+ databaseName, sqluser, sqlpw);
		} catch (SQLException e) {
			System.err.println(e);
		} catch (ClassNotFoundException ex) {
			Logger.getLogger(DatabaseInterface.class.getName()).log(
					Level.SEVERE, null, ex);
		}
		return con;
	}

	private void close(Connection con) {
		try {
			con.close();
		} catch (SQLException e) {
			System.err.println(e);
		}
	}

	// ===========
	// User
	// ===========

	public User getUser(String email) {
		User u = null;
		Connection con = null;
		try {
			con = openDataBase();
			u = user_mapper.getUser(email, con);
		} finally {
			close(con);
		}
		return u;
	}

	public User getUser(int userId) {
		User u = null;
		Connection con = null;
		try {
			con = openDataBase();
			u = user_mapper.getUser(userId, con);
		} finally {
			close(con);
		}
		return u;
	}

	public User getUserByDisplayName(String displayName) {
		User u = null;
		Connection con = null;
		try {
			con = openDataBase();
			u = user_mapper.getUserByDisplayName(displayName, con);
		} finally {
			close(con);
		}
		return u;
	}

	public ArrayList<User> getUsers() {
		Connection con = null;
		ArrayList<User> users = null;
		try {
			con = openDataBase();
			users = user_mapper.getUsers(con);
		} finally {
			close(con);
		}
		return users;
	}

	public boolean createUser(String displayName, String username,
			String password) {
		boolean result = false;
		Connection con = null;
		try {
			con = openDataBase();
			result = user_mapper.createUser(displayName, username, password,
					con);
		} finally {
			close(con);
		}
		return result;
	}

	public boolean deleteUser(int userId) {
		boolean result = false;
		Connection con = null;
		try {
			con = openDataBase();
			result = user_mapper.deleteUser(userId, con);
		} finally {
			close(con);
		}
		return result;
	}

	public boolean userExists(String username) {
		boolean result = false;
		Connection con = null;
		try {
			con = openDataBase();
			result = user_mapper.userExists(username, con);
		} finally {
			close(con);
		}
		return result;
	}

	public boolean displayNameExists(String displayName) {
		boolean result = false;
		Connection con = null;
		try {
			con = openDataBase();
			result = user_mapper.displayNameExists(displayName, con);
		} finally {
			close(con);
		}
		return result;
	}

	public void completeLogin(String email, String ipAddress) {
		Connection con = null;
		try {
			con = openDataBase();
			user_mapper.completeLogin(email, ipAddress, con);
		} finally {
			close(con);
		}
	}

	public void completeFailedLogin(int userId, String ipAddress) {
		Connection con = null;
		try {
			con = openDataBase();
			user_mapper.completeFailedLogin(userId, ipAddress, con);
		} finally {
			close(con);
		}
	}

	public boolean unlockUser(int userId) {
		Connection con = null;

		try {
			con = openDataBase();
			user_mapper.unlockUser(userId, con);
			return true;
		} catch (Exception e) {
			// TODO
		}
		return false;
	}

	public void updateProfile(User user) {
		Connection con = null;
		try {
			con = openDataBase();
			user_mapper.updateProfile(user, con);
		} finally {
			close(con);
		}
	}

	public void setRole(String username, String role) {
		Connection con = null;
		try {
			con = openDataBase();
			user_mapper.setRole(username, role, con);
		} finally {
			close(con);
		}
	}

	public void activateAccount(String email) {
		Connection con = null;
		try {
			con = openDataBase();
			user_mapper.activateAccount(email, con);
		} finally {
			close(con);
		}
	}

	public void changeUserPassword(String email, String password) {
		Connection con = null;
		try {
			con = openDataBase();
			user_mapper.changePassword(this.getUser(email).getId(), password,
					con);
		} finally {
			close(con);
		}
	}

	public void removeResetCode(String email) {
		Connection con = null;
		try {
			con = openDataBase();
			user_mapper.removeResetCode(email, con);
		} finally {
			close(con);
		}
	}

	public void setResetCode(String email, String code) {
		Connection con = null;
		try {
			con = openDataBase();
			user_mapper.setResetCode(email, code, con);
		} finally {
			close(con);
		}
	}

	// ===========
	// Users
	// ===========

	public ArrayList<User> SearchForPeople(String input) {
		ArrayList<User> arrayList = null;
		Connection con = null;
		try {
			con = openDataBase();
			arrayList = users_mapper.searchByName(input, con);
		} finally {
			close(con);
		}
		return arrayList;
	}

	public boolean hugPersonAsUser(int userId, int friendId) {
		Connection con = null;
		try {
			con = openDataBase();

			user_mapper.addHug(userId, friendId, con);
			return true;
		} finally {
			close(con);
		}
	}

	public boolean doesHugExists(int senderId, int receiverId) {
		boolean result = false;
		Connection con = null;
		try {
			con = openDataBase();
			result = user_mapper.doesHugExists(senderId, receiverId, con);
		} finally {
			close(con);
		}
		return result;
	}

	public ArrayList<User> searchByDisplayName(String displayName) {
		ArrayList<User> arrayList = null;
		Connection con = null;
		try {
			con = openDataBase();
			arrayList = users_mapper.searchByDisplayName(displayName, con);
		} finally {
			close(con);
		}

		return arrayList;
	}

	// ===========
	// User Relation
	// ===========

	public boolean hasRomance(int userId) {
		boolean result = false;
		Connection con = null;
		try {
			con = openDataBase();
			result = user_relation_mapper.hasRomance(userId, con);
		} finally {
			close(con);
		}
		return result;
	}

	public User findRomance(int userId) {
		User u = null;
		Connection con = null;
		try {
			con = openDataBase();
			u = user_relation_mapper.findRomance(userId, con);
		} finally {
			close(con);
		}
		return u;
	}

	public boolean removeRomance(int userId, int friendId) {
		boolean result = false;
		Connection con = null;
		try {
			con = openDataBase();
			result = user_relation_mapper.removeRomance(userId, friendId, con);
		} finally {
			close(con);
		}
		return result;
	}

	public ArrayList<User> getUserFriends(int userId) {
		ArrayList<User> arrayList = null;
		Connection con = null;
		try {
			con = openDataBase();
			arrayList = user_relation_mapper.findAllFriends(userId, con);
		} finally {
			close(con);
		}

		return arrayList;
	}

	public ArrayList<User> getUserNonFriends(int userId) {
		return this.getUserNonFriends(userId, null);
	}

	public ArrayList<User> getUserNonFriends(int userId, String displayName) {
		ArrayList<User> arrayList = null;
		Connection con = null;
		try {
			con = openDataBase();
			arrayList = user_relation_mapper.findAllNonFriends(userId,
					displayName, con);
		} finally {
			close(con);
		}

		return arrayList;
	}

	public boolean areFriends(int userId, int friendId) {
		boolean result = false;
		Connection con = null;
		try {
			con = openDataBase();
			result = user_relation_mapper.areFriends(userId, friendId, con);
		} finally {
			close(con);
		}
		return result;
	}

	public void addFriendToUser(int userId, int friendId) {
		Connection con = null;
		try {
			con = openDataBase();
			user_relation_mapper.addFriend(userId, friendId, con);
		} finally {
			close(con);
		}
	}

	public void removeFriendFromUser(int userId, int friendId) {
		Connection con = null;
		try {
			con = openDataBase();
			user_relation_mapper.removeFriend(userId, friendId, con);
		} finally {
			close(con);
		}
	}

	public ArrayList<User> findUncheckedHugs(int userId) {
		ArrayList<User> arrayList = null;
		Connection con = null;
		try {
			con = openDataBase();
			arrayList = user_mapper.findUncheckedHugs(userId, con);
		} finally {
			close(con);
		}

		return arrayList;
	}

	public boolean hasUncheckedHugs(int userId) {
		boolean result = false;
		Connection con = null;
		try {
			con = openDataBase();
			result = user_mapper.hasUncheckedHugs(userId, con);
		} finally {
			close(con);
		}
		return result;
	}

	// ===========
	// Romance
	// ===========

	public boolean hasPendingRomance(int senderId) {
		boolean result = false;
		Connection con = null;
		try {
			con = openDataBase();
			result = romance_mapper.hasPendingRomance(senderId, con);
		} finally {
			close(con);
		}
		return result;
	}

	public boolean hasDeclineRequest(int senderId) {
		boolean result = false;
		Connection con = null;
		try {
			con = openDataBase();
			result = romance_mapper.hasDeclineRequest(senderId, con);
		} finally {
			close(con);
		}
		return result;
	}

	public boolean requestRomance(int senderId, int targetId) {
		boolean result = false;
		Connection con = null;
		try {
			con = openDataBase();
			result = romance_mapper.requestRomance(senderId, targetId, con);
		} finally {
			close(con);
		}
		return result;
	}

	public User findPendingRomance(int senderId) {
		User u = null;
		Connection con = null;
		try {
			con = openDataBase();
			u = romance_mapper.findPendingRomance(senderId, con);
		} finally {
			close(con);
		}
		return u;
	}

	public boolean hasPendingRequest(int receiverId) {
		boolean result = false;
		Connection con = null;
		try {
			con = openDataBase();
			result = romance_mapper.hasPendingRequest(receiverId, con);
		} finally {
			close(con);
		}
		return result;
	}

	public ArrayList<User> findPendingRequests(int receiverId) {
		ArrayList<User> arrayList = null;
		Connection con = null;
		try {
			con = openDataBase();
			arrayList = romance_mapper.findPendingRequests(receiverId, con);
		} finally {
			close(con);
		}

		return arrayList;
	}

	public boolean declineRequest(int senderId, int receiverId) {
		boolean result = false;
		Connection con = null;
		try {
			con = openDataBase();
			result = romance_mapper.declineRequest(senderId, receiverId, con);
		} finally {
			close(con);
		}
		return result;
	}

	public boolean acceptRequest(int senderId, int receiverId) {
		boolean result = false;
		Connection con = null;
		try {
			con = openDataBase();
			result = romance_mapper.acceptRequest(senderId, receiverId, con);
		} finally {
			close(con);
		}
		return result;
	}

	public boolean readDeclineNotice(int senderId) {
		boolean result = false;
		Connection con = null;
		try {
			con = openDataBase();
			result = romance_mapper.readDeclineNotice(senderId, con);
		} finally {
			close(con);
		}
		return result;
	}

	public boolean acceptHug(int senderId, int receiverId) {
		boolean result = false;
		Connection con = null;
		try {
			con = openDataBase();
			result = user_mapper.acceptHug(senderId, receiverId, con);
		} finally {
			close(con);
		}
		return result;
	}

}
