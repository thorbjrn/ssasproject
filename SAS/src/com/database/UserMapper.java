package com.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.classes.Roles;
import com.classes.Tables;
import com.classes.User;
import com.utilities.Auth;
import com.utilities.Hasher;

/**
 * @author Ignas Jonikavicius
 */
public class UserMapper {

	public UserMapper() {
	}

	public User getUser(String email, Connection con) {
		String SQLselect = String.format(
				"SELECT * FROM %s WHERE user_email=? LIMIT 1", Tables.USERS);

		PreparedStatement getStatement = null;
		ResultSet resultset = null;

		try {
			getStatement = con.prepareStatement(SQLselect);
			getStatement.setString(1, email);
			resultset = getStatement.executeQuery();
			if (resultset.first()) {
				return new User(resultset);
			}
		} catch (SQLException e) {
			System.out.println("Fail in UserMapper - getUser(email)");
			System.out.println(e.getMessage());
		}

		return null;
	}

	public User getUser(int userId, Connection con) {
		String SQLselect = String.format(
				"SELECT * FROM %s WHERE id=? LIMIT 1", Tables.USERS);

		PreparedStatement getStatement = null;
		ResultSet resultset = null;

		try {
			getStatement = con.prepareStatement(SQLselect);
			getStatement.setInt(1, userId);
			resultset = getStatement.executeQuery();
			if (resultset.first()) {
				return new User(resultset);
			}
		} catch (SQLException e) {
			System.out.println("Fail in UserMapper - getUser(userId)");
			System.out.println(e.getMessage());
		}

		return null;
	}

	public User getUserByDisplayName(String displayName, Connection con) {
		String SQLselect = String.format(
				"SELECT * FROM %s WHERE display_name=? LIMIT 1", Tables.USERS);

		PreparedStatement getStatement = null;
		ResultSet resultset = null;

		try {
			getStatement = con.prepareStatement(SQLselect);
			getStatement.setString(1, displayName);
			resultset = getStatement.executeQuery();
			if (resultset.first()) {
				return new User(resultset);
			}
		} catch (SQLException e) {
			System.out.println("Fail in UserMapper - getUserByDisplayName");
			System.out.println(e.getMessage());
		}

		return null;
	}

	public ArrayList<User> getUsers(Connection con) {
		String SQLselect = String
				.format("SELECT * FROM %s where role_code != '%s' AND is_deleted is null",
						Tables.USERS, "ADMIN");

		PreparedStatement getStatement = null;
		ResultSet resultSet = null;
		ArrayList<User> users = new ArrayList<User>();

		try {
			getStatement = con.prepareStatement(SQLselect);
			resultSet = getStatement.executeQuery();
			while (resultSet.next())
				users.add(new User(resultSet));
			return users;

		} catch (SQLException e) {
			System.out.println("Fail in SessionMapper - getUsers");
			System.out.println(e.getMessage());
		}

		return null;
	}

	public boolean createUser(String displayName, String username,
			String password, Connection con) {
		String SQLinsert = String
				.format("INSERT INTO %s(display_name, user_email, user_password, role_code, activation_code) values(?, ?, ?, ?, ?)",
						Tables.USERS);
		PreparedStatement insertStatement = null;

		try {
			insertStatement = con.prepareStatement(SQLinsert);
			insertStatement.setString(1, displayName);
			insertStatement.setString(2, username);
			insertStatement.setString(3, password);
			insertStatement.setString(4, Roles.USER);
			insertStatement.setString(5, Hasher.toSHA256(username));
			// TODO:: create code generator for activation_code

			return (insertStatement.executeUpdate() == 1) ? true : false;
		} catch (SQLException e) {
			System.out.println("Fail in UserMapper - createUser");
			System.out.println(e.getMessage());
		}

		return false;
	}

	public boolean userExists(String username, Connection con) {
		String SQLselect = String.format(
				"SELECT COUNT(1) AS count FROM %s WHERE user_email=? ",
				Tables.USERS);

		PreparedStatement selectStatement = null;
		ResultSet resultSet = null;

		try {
			selectStatement = con.prepareStatement(SQLselect);
			selectStatement.setString(1, username);

			resultSet = selectStatement.executeQuery();
			if (resultSet.first()) {
				return resultSet.getInt(1) == 1;
			}
		} catch (SQLException e) {
			System.out.println("Fail in UserMapper - userExists");
			System.out.println(e.getMessage());
		}

		return false;
	}

	public boolean displayNameExists(String displayName, Connection con) {
		String SQLselect = String.format(
				"SELECT COUNT(1) AS count FROM %s WHERE display_name=? ",
				Tables.USERS);

		PreparedStatement selectStatement = null;
		ResultSet resultSet = null;

		try {
			selectStatement = con.prepareStatement(SQLselect);
			selectStatement.setString(1, displayName);

			resultSet = selectStatement.executeQuery();
			if (resultSet.first()) {
				return resultSet.getInt(1) == 1;
			}
		} catch (SQLException e) {
			System.out.println("Fail in UserMapper - displayNameExists");
			System.out.println(e.getMessage());
		}

		return false;
	}

	public void completeLogin(String email, String ipAddress, Connection con) {
		String SQLupdate = String
				.format("UPDATE %s SET count_login=count_login+1, try_until=5, last_login=?, last_login_ip=? WHERE user_email=?",
						Tables.USERS);

		PreparedStatement updateStatement = null;

		String currentDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
				.format(new Date());
		ipAddress = (ipAddress == null) ? "n/a" : ipAddress;
		//
		try {
			updateStatement = con.prepareStatement(SQLupdate);
			updateStatement.setString(1, currentDate);
			updateStatement.setString(2, ipAddress);
			updateStatement.setString(3, email);

			updateStatement.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Fail in UserMapper - completeLogin");
			System.out.println(e.getMessage());
		}
	}

	public void completeFailedLogin(int userId, String ipAddress, Connection con) {
		String SQLupdate = String
				.format("UPDATE %s SET try_until=try_until-1, is_locked=IF(try_until <= -1, 'Y', 'N'), last_login_ip=?, last_login=? WHERE id=?",
						Tables.USERS);

		PreparedStatement updateStatement = null;
		ipAddress = (ipAddress == null) ? "n/a" : ipAddress;
		String currentDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
		.format(new Date());

		try {
			updateStatement = con.prepareStatement(SQLupdate);
			updateStatement.setString(1, ipAddress);
			updateStatement.setString(2, currentDate);
			updateStatement.setInt(3, userId);

			updateStatement.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Fail in UserMapper - completeFailedLogin");
			System.out.println(e.getMessage());
		}
	}

	public void updateProfile(User user, Connection con) {
		String SQLupdate = String
				.format("UPDATE %s SET prefix=?, display_name=?, first_name=?, last_name=?, address=?, hobbies=?, about=?, share_name=?, share_address=? WHERE user_email=?",
						Tables.USERS);

		PreparedStatement updateStatement = null;

		try {
			updateStatement = con.prepareStatement(SQLupdate);
			updateStatement.setString(1, user.getPrefix());
			updateStatement.setString(2, user.getDisplay_name());
			updateStatement.setString(3, user.getFirst_name());
			updateStatement.setString(4, user.getLast_name());
			updateStatement.setString(5, user.getAddress());
			updateStatement.setString(6, user.getHobbies());
			updateStatement.setString(7, user.getAbout());
			updateStatement.setBoolean(8, user.getShare_name());
			updateStatement.setBoolean(9, user.getShare_address());
			updateStatement.setString(10, user.getUser_email());

			updateStatement.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Fail in UserMapper - updateProfile");
			System.out.println(e.getMessage());
		}
	}

	public void setRole(String username, String role, Connection con) {
		String SQLselect = String.format(
				"UPDATE users SET role_code = '%s' WHERE user_email = '%s'",
				role, username);

		PreparedStatement getStatement = null;

		try {
			getStatement = con.prepareStatement(SQLselect);
			getStatement.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Fail in UserMapper - setRole");
			System.out.println(e.getMessage());
		}
	}

	public void activateAccount(String email, Connection con) {
		String SQLupdate = String.format("UPDATE %s SET is_active='Y' WHERE user_email=?", Tables.USERS);
		
		PreparedStatement updateStatement = null;
		
		try {
			updateStatement = con.prepareStatement(SQLupdate);
			updateStatement.setString(1, email);

			updateStatement.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Fail in UserMapper - activateAccount");
			System.out.println(e.getMessage());
		}
	}
	
	public boolean deleteUser(int userId, Connection con) {
		String SQLupdate = String.format(
				"UPDATE users SET is_deleted=NOW() WHERE id = '%s'", userId);

		PreparedStatement updateStatement = null;

		try {
			updateStatement = con.prepareStatement(SQLupdate);
			updateStatement.executeUpdate();

			return (updateStatement.executeUpdate() == 1);
		} catch (SQLException e) {
			System.out.println("Fail in UserMapper - deleteUser");
			System.out.println(e.getMessage());
		}
		return false;
	}

	public boolean changePassword(int userId, String password, Connection con) { //changes a users password.
		String SQLupdate = String.format("UPDATE %s SET user_password=? WHERE id=?", Tables.USERS);
		
		PreparedStatement updateStatement = null;
		
		try {
			updateStatement = con.prepareStatement(SQLupdate);
			updateStatement.setString(1, Auth.hash_password(password));
			updateStatement.setString(2, String.valueOf(userId));
			updateStatement.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Fail in UserMapper - changePassword");
			System.out.println(e.getMessage());
			return false;
		}
		return true;
	}
	
	
	public Boolean setResetCode(String email, String code, Connection con) { //Sets the reset code.
		String SQLupdate = String.format("UPDATE %s SET reset_code=? WHERE user_email=?", Tables.USERS);
		
		PreparedStatement updateStatement = null;
		
		try {
			updateStatement = con.prepareStatement(SQLupdate);
			updateStatement.setString(1, code);
			updateStatement.setString(2, email);
			updateStatement.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Fail in UserMapper - setResetCode");
			System.out.println(e.getMessage());
			return false;
		}
		return true;
	}
	
	public Boolean removeResetCode(String email, Connection con) { //reset code to NULL in db.
		String SQLupdate = String.format("UPDATE %s SET reset_code=NULL WHERE user_email=?", Tables.USERS);
		
		PreparedStatement updateStatement = null;
		
		try {
			updateStatement = con.prepareStatement(SQLupdate);
			updateStatement.setString(1, email);
			updateStatement.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Fail in UserMapper - removeResetCode");
			System.out.println(e.getMessage());
			return false;
		}
		return true;
	}
	
	/**
	 * Unlock user's account. Sets is_locked to 'N' and try_until to 5
	 * 
	 * @param userId
	 * @param con
	 * @return
	 */
	public boolean unlockUser(int userId, Connection con) {
		String SQLupdate = String.format(
				"UPDATE %s SET try_until=?, is_locked=? WHERE id=?",
				Tables.USERS);
		PreparedStatement updateStatement = null;
		try {
			updateStatement = con.prepareStatement(SQLupdate);
			updateStatement.setInt(1, 5);
			updateStatement.setString(2, "N");
			updateStatement.setInt(3, userId);
			updateStatement.executeUpdate();

			return (updateStatement.executeUpdate() == 1);
		} catch (SQLException e) {
			System.out.println("Fail in UserMapper - unlockUser");
			System.out.println(e.getMessage());
		}

		return false;
	}

	public ArrayList<User> findUncheckedHugs(int userId, Connection con) {
		ArrayList<User> arrayList = new ArrayList<User>();
		String SQLselect = String
				.format("SELECT u.id, u.display_name, u.about FROM %s u, %s h WHERE h.receiver = ? AND h.seen = '0' AND u.id = h.sender",
				Tables.USERS, Tables.HUGS);
		String[] cols = new String[] { "id", "display_name", "about" };
		
		
		PreparedStatement getStatement = null;
		ResultSet resultSet = null;

		try {
			getStatement = con.prepareStatement(SQLselect);
			getStatement.setInt(1, userId);

			resultSet = getStatement.executeQuery();
			while (resultSet.next()) {
				arrayList.add(new User(resultSet, cols));
			}
		} catch (SQLException e) {
			System.out.println("Fail in UserMapper - findUncheckedHugs");
			System.out.println(e.getMessage());
		}

		return arrayList;
	
	}

	public boolean hasUncheckedHugs(int userId, Connection con) {
		String SQLselect = String
		.format("SELECT COUNT(1) AS count FROM %s WHERE receiver=? AND seen='0'",
				Tables.HUGS);
		
		PreparedStatement selectStatement = null;
		ResultSet resultSet = null;
		
		try {
			selectStatement = con.prepareStatement(SQLselect);
			selectStatement.setInt(1, userId);
	
			resultSet = selectStatement.executeQuery();
			if (resultSet.first()) {
				return resultSet.getInt(1) > 0;
			}
		} catch (SQLException e) {
			System.out.println("Fail in UserMapper - hasUncheckedHugs");
			System.out.println(e.getMessage());
		}

		return false;
	}
	
	public void addHug(int userId, int friendId, Connection con) {		
		String SQLinsert = String.format(
				"REPLACE INTO %s SET sender=?, receiver=?, seen=0", Tables.HUGS);
	    
		PreparedStatement insertStatement = null;

		try {
			insertStatement = con.prepareStatement(SQLinsert);
			insertStatement.setInt(1, userId);
			insertStatement.setInt(2, friendId);

			insertStatement.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Fail in UserMapper - addHug");
			System.out.println(e.getMessage());
		}
	}

	public boolean doesHugExists(int senderId, int receiverId, Connection con) {		
		String SQLselect = String.format(
				"SELECT COUNT(1) AS count FROM %s WHERE sender=? AND receiver=? AND seen=0",
				Tables.HUGS);

		PreparedStatement selectStatement = null;
		ResultSet resultSet = null;
		
		try {
			selectStatement = con.prepareStatement(SQLselect);
			selectStatement.setInt(1, senderId);
			selectStatement.setInt(2, receiverId);

			resultSet = selectStatement.executeQuery();
			if (resultSet.first()) {
				return resultSet.getInt(1) == 1;
			}
		} catch (SQLException e) {
			System.out.println("Fail in UserMapper - alreadyHuggedPerson");
			System.out.println(e.getMessage());
		}

		return false;
	}

	public boolean acceptHug(int senderId, int receiverId, Connection con) {
		String SQLupdate = String
				.format("UPDATE %s SET seen='1' WHERE sender=? AND receiver=? AND seen='0'",
						Tables.HUGS);

		PreparedStatement updateStatement = null;

		try {
			updateStatement = con.prepareStatement(SQLupdate);
			updateStatement.setInt(1, senderId);
			updateStatement.setInt(2, receiverId);

			return (updateStatement.executeUpdate() == 1);
		} catch (SQLException e) {
			System.out.println("Fail in UserMapper - acceptHug");
			System.out.println(e.getMessage());
		}
		return false;
	}

}
