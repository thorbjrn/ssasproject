package com.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.classes.Tables;
import com.classes.User;

/**
 * @author Ignas Jonikavicius
 */
public class RomanceMapper {

	private static String PENDING = "PENDING";
	private static String ACCEPTED = "ACCEPTED";
	private static String DECLINED = "DECLINED";

	/**
	 * Check if user has issued a new romance request
	 */
	public boolean hasPendingRomance(int senderId, Connection con) {
		String SQLselect = String
				.format("SELECT COUNT(1) AS count FROM %s WHERE sender=? AND status='%s'",
						Tables.ROMANCE_REQUEST, PENDING);

		PreparedStatement selectStatement = null;
		ResultSet resultSet = null;

		try {
			selectStatement = con.prepareStatement(SQLselect);
			selectStatement.setInt(1, senderId);

			resultSet = selectStatement.executeQuery();
			if (resultSet.first()) {
				return resultSet.getInt(1) > 0;
			}
		} catch (SQLException e) {
			System.out.println("Fail in RomanceMapper - hasPendingRomance");
			System.out.println(e.getMessage());
		}

		return false;
	}

	/**
	 * Check if user has recently declined romance request
	 */
	public boolean hasDeclineRequest(int senderId, Connection con) {
		String SQLselect = String
				.format("SELECT COUNT(1) AS count FROM %s WHERE sender=? AND status='%s' AND read_response IS NULL",
						Tables.ROMANCE_REQUEST, DECLINED);

		PreparedStatement selectStatement = null;
		ResultSet resultSet = null;

		try {
			selectStatement = con.prepareStatement(SQLselect);
			selectStatement.setInt(1, senderId);

			resultSet = selectStatement.executeQuery();
			if (resultSet.first()) {
				return resultSet.getInt(1) > 0;
			}
		} catch (SQLException e) {
			System.out.println("Fail in RomanceMapper - hasDeclineRequest");
			System.out.println(e.getMessage());
		}

		return false;
	}
	
	/**
	 * Issue a new romance request
	 */
	public boolean requestRomance(int senderId, int targetId, Connection con) {
		String SQLinsert = String.format(
				"INSERT INTO %s(sender, receiver, status) values(?, ?, '%s')",
				Tables.ROMANCE_REQUEST, PENDING);
		PreparedStatement insertStatement = null;

		try {
			insertStatement = con.prepareStatement(SQLinsert);
			insertStatement.setInt(1, senderId);
			insertStatement.setInt(2, targetId);

			return (insertStatement.executeUpdate() == 1) ? true : false;
		} catch (SQLException e) {
			System.out.println("Fail in RomanceMapper - requestRomance");
			System.out.println(e.getMessage());
		}

		return false;
	}

	/**
	 * Return sender's pending Romance
	 */
	public User findPendingRomance(int senderId, Connection con) {
		String SQLselect = String
				.format("SELECT * FROM %s WHERE id=(SELECT receiver FROM %s WHERE sender=? AND status='%s') LIMIT 1",
						Tables.USERS, Tables.ROMANCE_REQUEST, PENDING);

		PreparedStatement getStatement = null;
		ResultSet resultset = null;

		try {
			getStatement = con.prepareStatement(SQLselect);
			getStatement.setInt(1, senderId);
			resultset = getStatement.executeQuery();
			if (resultset.first()) {
				return new User(resultset);
			}
		} catch (SQLException e) {
			System.out.println("Fail in RomanceMapper - findPendingRomance");
			System.out.println(e.getMessage());
		}

		return null;
	}

	/**
	 * Check if receiver has any pending romance requests
	 */
	public boolean hasPendingRequest(int receiverId, Connection con) {
		String SQLselect = String
				.format("SELECT COUNT(1) AS count FROM %s WHERE receiver=? AND status='%s'",
						Tables.ROMANCE_REQUEST, PENDING);

		PreparedStatement selectStatement = null;
		ResultSet resultSet = null;

		try {
			selectStatement = con.prepareStatement(SQLselect);
			selectStatement.setInt(1, receiverId);

			resultSet = selectStatement.executeQuery();
			if (resultSet.first()) {
				return resultSet.getInt(1) > 0;
			}
		} catch (SQLException e) {
			System.out.println("Fail in RomanceMapper - hasPendingRequest");
			System.out.println(e.getMessage());
		}

		return false;
	}

	/**
	 * Find all users that have issued romance request to receiver
	 */
	public ArrayList<User> findPendingRequests(int receiverId, Connection con) {
		ArrayList<User> arrayList = new ArrayList<User>();
		String SQLselect = String
				.format("SELECT u.id, u.display_name, u.about FROM %s u, %s rr WHERE rr.receiver = ? AND rr.status = '%s' AND u.id = rr.sender",
						Tables.USERS, Tables.ROMANCE_REQUEST, PENDING);
		String[] cols = new String[] { "id", "display_name", "about" };

		PreparedStatement getStatement = null;
		ResultSet resultSet = null;

		try {
			getStatement = con.prepareStatement(SQLselect);
			getStatement.setInt(1, receiverId);

			resultSet = getStatement.executeQuery();
			while (resultSet.next()) {
				arrayList.add(new User(resultSet, cols));
			}
		} catch (SQLException e) {
			System.out.println("Fail in RomanceMapper - findPendingRequests");
			System.out.println(e.getMessage());
		}

		return arrayList;
	}

	/**
	 * Decline Request
	 */
	public boolean declineRequest(int senderId, int receiverId, Connection con) {
		String SQLupdate = String
				.format("UPDATE %s SET status='%s' WHERE sender=? AND receiver=? AND status='%s'",
						Tables.ROMANCE_REQUEST, DECLINED, PENDING);

		PreparedStatement updateStatement = null;

		try {
			updateStatement = con.prepareStatement(SQLupdate);
			updateStatement.setInt(1, senderId);
			updateStatement.setInt(2, receiverId);

			return (updateStatement.executeUpdate() == 1);
		} catch (SQLException e) {
			System.out.println("Fail in RomanceMapper - declineRequest");
			System.out.println(e.getMessage());
		}
		return false;
	}
	
	/**
	 * Accept Request
	 */
	public boolean acceptRequest(int senderId, int receiverId, Connection con) {
		String SQLupdate = String
				.format("UPDATE %s SET status='%s' WHERE sender=? AND receiver=? AND status='%s'",
						Tables.ROMANCE_REQUEST, ACCEPTED, PENDING);

		PreparedStatement updateStatement = null;

		try {
			updateStatement = con.prepareStatement(SQLupdate);
			updateStatement.setInt(1, senderId);
			updateStatement.setInt(2, receiverId);

			return (updateStatement.executeUpdate() == 1);
		} catch (SQLException e) {
			System.out.println("Fail in RomanceMapper - acceptRequest");
			System.out.println(e.getMessage());
		}
		return false;
	}

	/**
	 * Set request response to read
	 */
	public boolean readDeclineNotice(int senderId, Connection con) {
		String SQLupdate = String
				.format("UPDATE %s SET read_response=NOW() WHERE sender=? AND status='%s' AND read_response IS NULL",
						Tables.ROMANCE_REQUEST, DECLINED);

		PreparedStatement updateStatement = null;

		try {
			updateStatement = con.prepareStatement(SQLupdate);
			updateStatement.setInt(1, senderId);

			return (updateStatement.executeUpdate() == 1);
		} catch (SQLException e) {
			System.out.println("Fail in RomanceMapper - readDeclineNotice");
			System.out.println(e.getMessage());
		}
		return false;
	}
}
