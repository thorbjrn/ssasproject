package com.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.classes.Tables;
import com.classes.User;

public class UsersMapper {

	public UsersMapper() {
	}

	/**
	 * Returns a list of Users with matching name
	 */
	public ArrayList<User> searchByName(String input, Connection con) {
		ArrayList<User> arrayList = new ArrayList<User>();
		String SQLselect = String
				.format("SELECT id, first_name, last_name FROM %s WHERE first_name LIKE ?",
						Tables.USERS);
		String[] cols = new String[] { "id", "first_name", "last_name" };

		PreparedStatement getStatement = null;
		ResultSet resultSet = null;

		try {
			getStatement = con.prepareStatement(SQLselect);
			getStatement.setString(1, "%" + input + "%");

			resultSet = getStatement.executeQuery();
			while (resultSet.next()) {
				arrayList.add(new User(resultSet, cols));
			}
		} catch (SQLException e) {
			System.out.println("Fail in UsersMapper - searchByName");
			System.out.println(e.getMessage());
		}

		return arrayList;
	}
	
	
	public ArrayList<User> searchByDisplayName(String displayName, Connection con) {
		ArrayList<User> arrayList = new ArrayList<User>();
		String SQLselect = String
				.format("SELECT id, display_name, about, hobbies FROM %s u where display_name LIKE ? AND role_code != '%s';",
						Tables.USERS, "ADMIN");

		String[] cols = new String[] { "id", "display_name", "about", "hobbies" };

		PreparedStatement getStatement = null;
		ResultSet resultSet = null;

		try {
			getStatement = con.prepareStatement(SQLselect);
			getStatement.setString(1, "%" + displayName + "%");

			resultSet = getStatement.executeQuery();
			while (resultSet.next()) {
				arrayList.add(new User(resultSet, cols));
			}

		} catch (SQLException e) {
			System.out.println("Fail in UsersMapper - searchByDisplayName");
			System.out.println(e.getMessage());
		}

		return arrayList;
	}

}
