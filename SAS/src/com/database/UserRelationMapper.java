package com.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.classes.Tables;
import com.classes.User;

public class UserRelationMapper {

	/**
	 * Checks if given user has a romance
	 */
	public boolean hasRomance(int userId, Connection con) {
		String SQLselect = String.format(
				"SELECT COUNT(1) AS count FROM %s WHERE uid=? AND romance=1",
				Tables.USER_RELATION);

		PreparedStatement selectStatement = null;
		ResultSet resultSet = null;

		try {
			selectStatement = con.prepareStatement(SQLselect);
			selectStatement.setInt(1, userId);

			resultSet = selectStatement.executeQuery();
			if (resultSet.first()) {
				return resultSet.getInt(1) > 0;
			}
		} catch (SQLException e) {
			System.out.println("Fail in UsersMapper - hasRomance");
			System.out.println(e.getMessage());
		}

		return false;
	}

	/**
	 * Return User Romance
	 */
	public User findRomance(int userId, Connection con) {
		String SQLselect = String
				.format("SELECT * FROM %s WHERE id=(SELECT fid FROM %s WHERE uid=? AND romance=1 LIMIT 1) LIMIT 1",
						Tables.USERS, Tables.USER_RELATION);

		PreparedStatement getStatement = null;
		ResultSet resultset = null;

		try {
			getStatement = con.prepareStatement(SQLselect);
			getStatement.setInt(1, userId);
			resultset = getStatement.executeQuery();
			if (resultset.first()) {
				return new User(resultset);
			}
		} catch (SQLException e) {
			System.out.println("Fail in UsersMapper - findRomance");
			System.out.println(e.getMessage());
		}

		return null;
	}

	/**
	 * Remove Romance between users
	 */
	public boolean removeRomance(int userId, int friendId, Connection con) {
		String SQLupdate = String
				.format("UPDATE %s SET romance=0 WHERE (uid=? AND fid=?) OR (uid=? AND fid=?)",
						Tables.USER_RELATION);

		PreparedStatement updateStatement = null;

		try {
			updateStatement = con.prepareStatement(SQLupdate);
			updateStatement.setInt(1, userId);
			updateStatement.setInt(2, friendId);
			updateStatement.setInt(3, friendId);
			updateStatement.setInt(4, userId);

			return (updateStatement.executeUpdate() == 1);
		} catch (SQLException e) {
			System.out.println("Fail in UserRelationMapper - removeRomance");
			System.out.println(e.getMessage());
		}
		return false;
	}

	/**
	 * Returns a list of all user's friends
	 */
	public ArrayList<User> findAllFriends(int userId, Connection con) {
		ArrayList<User> arrayList = new ArrayList<User>();
		String SQLselect = String
				.format("SELECT u.id, u.display_name, u.about, u.hobbies FROM %s u, %s ur WHERE ur.uid = ? AND u.id = ur.fid",
						Tables.USERS, Tables.USER_RELATION);
		String[] cols = new String[] { "id", "display_name", "about", "hobbies" };

		PreparedStatement getStatement = null;
		ResultSet resultSet = null;

		try {
			getStatement = con.prepareStatement(SQLselect);
			getStatement.setInt(1, userId);

			resultSet = getStatement.executeQuery();
			while (resultSet.next()) {
				arrayList.add(new User(resultSet, cols));
			}
		} catch (SQLException e) {
			System.out.println("Fail in UsersMapper - findAllFriends");
			System.out.println(e.getMessage());
		}

		return arrayList;
	}

	/**
	 * Returns a list of all user's non friends. Matches display name
	 */
	public ArrayList<User> findAllNonFriends(int userId, String displayName,
			Connection con) {
		ArrayList<User> arrayList = new ArrayList<User>();
		
		String SQLselect = "";
		if (displayName == null) {
			SQLselect = String
					.format("SELECT id, display_name, about, hobbies FROM %s u where id != ? AND id NOT IN(select fid from %s where uid = ?) AND role_code != '%s';",
							Tables.USERS, Tables.USER_RELATION, "ADMIN");
		} else {
			SQLselect = String
					.format("SELECT id, display_name, about, hobbies FROM %s u where id != ? AND id NOT IN(select fid from %s where uid = ?) AND display_name LIKE ? AND role_code != '%s';",
							Tables.USERS, Tables.USER_RELATION, "ADMIN");
		}

		String[] cols = new String[] { "id", "display_name", "about", "hobbies" };

		PreparedStatement getStatement = null;
		ResultSet resultSet = null;

		try {
			getStatement = con.prepareStatement(SQLselect);
			getStatement.setInt(1, userId);
			getStatement.setInt(2, userId);
			if (displayName != null)
				getStatement.setString(3, "%" + displayName + "%");

			resultSet = getStatement.executeQuery();
			while (resultSet.next()) {
				arrayList.add(new User(resultSet, cols));
			}

		} catch (SQLException e) {
			System.out.println("Fail in UsersMapper - findAllFriends");
			System.out.println(e.getMessage());
		}

		return arrayList;
	}

	/**
	 * Check if users are friends
	 */
	public boolean areFriends(int userId, int friendId, Connection con) {
		String SQLselect = String
				.format("SELECT COUNT(1) AS count FROM %s WHERE uid=? AND fid=? LIMIT 1",
						Tables.USER_RELATION);

		PreparedStatement selectStatement = null;
		ResultSet resultSet = null;

		try {
			selectStatement = con.prepareStatement(SQLselect);
			selectStatement.setInt(1, userId);
			selectStatement.setInt(2, friendId);

			resultSet = selectStatement.executeQuery();
			if (resultSet.first()) {
				return resultSet.getInt(1) == 1;
			}
		} catch (SQLException e) {
			System.out.println("Fail in UsersMapper - hasRomance");
			System.out.println(e.getMessage());
		}

		return false;
	}

	/**
	 * Adds friend to user
	 */
	public void addFriend(int userId, int friendId, Connection con) {
		String SQLinsert = String.format(
				"INSERT INTO %s(uid, fid) values(?,?)", Tables.USER_RELATION);

		PreparedStatement insertStatement = null;

		try {
			insertStatement = con.prepareStatement(SQLinsert);
			insertStatement.setInt(1, userId);
			insertStatement.setInt(2, friendId);

			insertStatement.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Fail in UsersMapper - addFriend");
			System.out.println(e.getMessage());
		}
	}

	/**
	 * Remove friend from user
	 */
	public void removeFriend(int userId, int friendId, Connection con) {
		String SQLinsert = String.format(
				"DELETE FROM %s WHERE uid=? AND fid=?", Tables.USER_RELATION);

		PreparedStatement insertStatement = null;

		try {
			insertStatement = con.prepareStatement(SQLinsert);
			insertStatement.setInt(1, userId);
			insertStatement.setInt(2, friendId);

			insertStatement.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Fail in UsersMapper - removeFriend");
			System.out.println(e.getMessage());
		}
	}

}
