package com.validators;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 * 
 * @author Ignas Jonikavicius
 * 
 *         Checks if 'Username' input field has a valid value
 */

@FacesValidator("com.UsernameValidator")
@SuppressWarnings("all")
public class UsernameValidator implements Validator {

	private Severity severity = FacesMessage.SEVERITY_ERROR;
	private String summary = "Username validation failed.";

	private int minLength = 9; // *@itu.com
	private int maxLength = 254;

	public static String[] usrComplexityInfo = new String[] {
			"Use your email address as Username",
			"Accepted email domains: @itu.dk and @itu.com" };

	@Override
	public void validate(FacesContext context, UIComponent component,
			Object oValue) throws ValidatorException {

		String value = oValue.toString();

		if (!Validate.notEmpty(value)) {
			throw new ValidatorException(new FacesMessage(severity, summary,
					"Field is empty."));
		}

		if (!Validate.minLength(value, minLength)) {
			throw new ValidatorException(new FacesMessage(severity, summary,
					"Too short."));
		}

		if (!Validate.maxLength(value, maxLength)) {
			throw new ValidatorException(new FacesMessage(severity, summary,
					"Too long."));
		}

		if (!Validate.email(value)) {
			throw new ValidatorException(new FacesMessage(severity, summary,
					"Invalid Username format."));
		}

		if (!ituEmail(value)) {
			throw new ValidatorException(new FacesMessage(severity, summary,
					"Invalid email domain."));
		}

	}

	private boolean ituEmail(String value) {
		if (value.split("@").length == 2 && value.split("@")[1] != null) {
			String domain = value.split("@")[1];
			if (domain.equalsIgnoreCase("itu.dk")
					|| domain.equalsIgnoreCase("itu.com"))
				return true;
		}

		return false;
	}
}
