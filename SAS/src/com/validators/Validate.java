package com.validators;

import org.apache.commons.validator.routines.EmailValidator;

/**
 * 
 * @author Ignas Jonikavicius
 * 
 *         Stores a number of static functions to do basic validation on value.
 */
public class Validate {

	/**
	 * Checks if a vlua is empty string.
	 * 
	 * @param value
	 * @return
	 */
	public static boolean notEmpty(String value) {
		return !value.trim().isEmpty();
	}

	/**
	 * Checks whether a string consists of alphabetical characters and numbers
	 * only.
	 * 
	 * @param value
	 * @return
	 */
	public static boolean alphaNumeric(String value) {
		return value.matches("[A-Za-z0-9]+");
	}

	public static boolean decimal(String value) {
		return decimal(value, 0, 0);
	}

	public static boolean decimal(String value, int places) {
		return decimal(value, places, 0);
	}

	/**
	 * Checks if a string is a proper decimal format. Optionally, a specific
	 * number of digits can be checked too.
	 * 
	 * @param value
	 * @param places
	 * @param digits
	 * @return
	 */
	public static boolean decimal(String value, int places, int digits) {
		String digitsNo = "+"; // any no of digits
		if (digits > 0) {
			digitsNo = "{" + digits + "}"; // specific no of digits
		}

		places = (places > 0) ? places : 2;

		return value
				.matches("^[+-]?\\d" + digitsNo + "\\.\\d{" + places + "}$");
	}

	/**
	 * Checks whether a string consists of digits only (no dots or dashes).
	 * 
	 * @param value
	 * @return
	 */
	public static boolean digit(String value) {
		return value.matches("^\\d+$");
	}

	/**
	 * Checks whether a string is a valid number (negative and decimal numbers
	 * allowed).
	 * 
	 * @param value
	 * @return
	 */
	public static boolean numeric(String value) {
		return value.matches("^-?+(?=.*[0-9])[0-9]*+\\.?+[0-9]*+$");
	}

	/**
	 * Tests if a number is within a range.
	 * 
	 * @param value
	 * @param min
	 * @param max
	 * @return
	 */
	public static boolean range(String value, float min, float max) {
		try {
			float fValue = Float.parseFloat(value);
			return (fValue >= min && fValue <= max);
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Checks a value against a regular expression.
	 * 
	 * @param value
	 * @param expression
	 * @return
	 */
	public static boolean regex(String value, String expression) {
		return value.matches(expression);
	}

	/**
	 * Checks if a value is a valid email address
	 * 
	 * @param value
	 * @return
	 */
	public static boolean email(String value) {
		return EmailValidator.getInstance().isValid(value);
	}

	/**
	 * Checks that a value is short enough.
	 * 
	 * @param value
	 * @param length
	 * @return
	 */
	public static boolean maxLength(String value, int length) {
		return value.trim().length() <= length;
	}

	/**
	 * Checks that a value is long enough.
	 * 
	 * @param value
	 * @param length
	 * @return
	 */
	public static boolean minLength(String value, int length) {
		return value.trim().length() >= length;
	}
}
