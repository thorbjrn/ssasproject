package com.validators;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator("com.PasswordValidator")
@SuppressWarnings("all")
public class PasswordValidator implements Validator {

	private Severity severity = FacesMessage.SEVERITY_ERROR;
	private String summary = "Password validation failed.";

	private int minLength = 8;
	private int maxLength = 100;
	private String pswComplexity = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=.])(?=\\S+$).{8,}$";

	public static String[] pswComplexityInfo = new String[] {
			"At least 8 characters", "Contains at least one digit",
			"Contains at least one lower alphabetic character - a-z",
			"Contains at least one upper alphabetic character - A-Z",
			"Contains at least one special character - @#$%^&+=.",
			"Not containing blank, tab etc." };

	// ^ # start-of-string
	// (?=.*[0-9]) # a digit must occur at least once
	// (?=.*[a-z]) # a lower case letter must occur at least once
	// (?=.*[A-Z]) # an upper case letter must occur at least once
	// (?=.*[@#$%^&+=.]) # a special character must occur at least once
	// (?=\S+$) # no whitespace allowed in the entire string
	// .{8,} # anything, at least eight places though
	// $ # end-of-string

	@Override
	public void validate(FacesContext context, UIComponent component,
			Object o) throws ValidatorException {

		String value = o.toString();

		if (!Validate.notEmpty(value)) {
			throw new ValidatorException(new FacesMessage(severity, summary,
					"Field is empty."));
		}

		if (!Validate.minLength(value, minLength)) {
			throw new ValidatorException(new FacesMessage(severity, summary,
					"Too short. Must be at least " + minLength
							+ " characters long."));
		}

		if (!Validate.maxLength(value, maxLength)) {
			throw new ValidatorException(new FacesMessage(severity, summary,
					"Too long. Must not exceed " + maxLength + " characters."));
		}

		if (!Validate.regex(value, pswComplexity)) {
			throw new ValidatorException(new FacesMessage(severity, summary,
					"Invalid format."));
		}
	}

}
