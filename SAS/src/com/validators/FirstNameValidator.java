package com.validators;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator("com.FirstNameValidator")
public class FirstNameValidator implements Validator {

	private Severity severity = FacesMessage.SEVERITY_ERROR;
	private String summary = "First Name validation failed.";

	private int maxLength = 50;

	@Override
	public void validate(FacesContext context, UIComponent component, Object o)
			throws ValidatorException {

		String value = o.toString();

		if (!Validate.notEmpty(value)) {
			throw new ValidatorException(new FacesMessage(severity, summary,
					"Field is empty."));
		}

		if (!Validate.maxLength(value, maxLength)) {
			throw new ValidatorException(new FacesMessage(severity, summary,
					"Too long. Must not exceed " + maxLength + " characters."));
		}
		
		if (!Validate.regex(value, "^[a-zA-Z\\s'.-]+$")) {
			throw new ValidatorException(new FacesMessage(severity, summary,
					"Invalid Format. Only letters, spaces and '.- allowed"));
		}

	}

}
