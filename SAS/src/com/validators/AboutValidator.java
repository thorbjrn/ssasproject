package com.validators;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator("com.AboutValidator")
public class AboutValidator implements Validator {

	private Severity severity = FacesMessage.SEVERITY_ERROR;
	private String summary = "About validation failed.";

	private int maxLength = 500;

	@Override
	public void validate(FacesContext context, UIComponent component, Object o)
			throws ValidatorException {

		String value = o.toString();

		if (!Validate.maxLength(value, maxLength)) {
			throw new ValidatorException(new FacesMessage(severity, summary,
					"Too long. Must not exceed " + maxLength + " characters."));
		}

	}

}
