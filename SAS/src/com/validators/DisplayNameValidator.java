package com.validators;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import com.database.DatabaseInterface;
import com.database.UserMapper;

@FacesValidator("com.DisplayNameValidator")
public class DisplayNameValidator implements Validator {

	private Severity severity = FacesMessage.SEVERITY_ERROR;
	private String summary = "Display Name validation failed.";

	private int maxLength = 50;
	private int minLength = 3;

	public static String[] dnComplexityInfo = new String[] {
			"At least 3 characters",
			"Can containt letters, numbers and -_!? characters",
			"Not containing blank, tab etc." };

	@Override
	public void validate(FacesContext context, UIComponent component, Object o)
			throws ValidatorException {

		String value = o.toString();

		if (!Validate.notEmpty(value)) {
			throw new ValidatorException(new FacesMessage(severity, summary,
					"Field is empty."));
		}

		if (!Validate.minLength(value, minLength)) {
			throw new ValidatorException(new FacesMessage(severity, summary,
					"Too short. Must be at least " + minLength
							+ " characters long."));
		}

		if (!Validate.maxLength(value, maxLength)) {
			throw new ValidatorException(new FacesMessage(severity, summary,
					"Too long. Must not exceed " + maxLength + " characters."));
		}

		if (!Validate.regex(value, "^[a-zA-Z0-9-_!?]+$")) {
			throw new ValidatorException(new FacesMessage(severity, summary,
					"Invalid Format. Only letters, numbers and -_!? allowed"));
		}
		

	}

}
