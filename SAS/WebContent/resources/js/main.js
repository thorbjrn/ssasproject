$(function() {
	/**
	 * Hide error when field was focused
	 */
	$(".input-group input").bind({
		focus : function() {
			// remove error
			$(this).parent().find('.err').remove();
			// show info
			$(this).parent().find('.info').show();
		},
		blur : function() {
			// hide info
			$(this).parent().find('.info').hide();
		}
	});
	/**
	 * Navigation
	 */
	$('#nav a').each(function() {
		if ($(this).attr('href') == window.location.pathname)
			$(this).addClass('active');
	});
	/**
	 * Global messages
	 */
	$('.messages > .info').parent().delay(3000).fadeOut("slow");
});

var options = {
	team1 : {
		uri : 'https://192.237.215.145',
		id : '#team-1',
		services : {
			users : '/ssasf14/service/users'
		}
	},
	team2 : {
		uri : 'https://166.78.184.170/service',
		id : '#team-2',
		services : {
			users : '/users',
			user : '/users/{id}'
		}
	}
};

var serviceUri = "https://192.237.211.45/ssasf14/" //"http://localhost:8080/SAS";

function showAjaxError(jqXHR, ajaxOptions, thrownError) {
	if (jqXHR.status === 0) {
		console.log('Not connect.\n Verify Network.');
	} else if (jqXHR.status == 400) {
		console.log('Bad request. [400]');
	} else if (jqXHR.status == 404) {
		console.log('Requested page not found. [404]');
	} else if (jqXHR.status == 500) {
		console.log('Internal Server Error [500].');
	} else if (ajaxOptions === 'parsererror') {
		console.log('Requested JSON parse failed.');
	} else if (ajaxOptions === 'timeout') {
		console.log('Time out error.');
	} else if (ajaxOptions === 'abort') {
		console.log('Ajax request aborted.');
	} else {
		console.log('Uncaught Error.\n' + jqXHR.thrownError);
	}
}

function getUsers(teamId) {
	var params = options[teamId];

	var $table = $(params.id);
	var $tbody = $('tbody', $table);
	
	this.xhr = $.ajax({
		url : serviceUri + "/services?url=" + params.uri
				+ params.services.users,
		type : 'GET',
		dataType : 'json',
		success : function(data, textStatus, XMLHttpRequest) {
			var rows = [];
			if (teamId == 'team1') {
				$.each(data.users, function(i, item) {
					var td = $('<td>', {
						text : item.name
					});
					td.on('click', function() {
						getUser1(item.link, teamId);
					});
	
					rows.push($('<tr>').append(td));
				});
			}
			$tbody.append(rows);
		},
		error : function(jqXHR, ajaxOptions, thrownError) {
			showAjaxError(jqXHR, ajaxOptions, thrownError);

			$tbody.append($('<tr>').append($('<td>', {
				colspan : 2,
				text : 'No records found.'
			})));
		}
	});
}

function getUser1(link, teamId) {
	var params = options[teamId];

	var $user = $('#affiliate-user');
	var $nick = $('#nick', $user);
	var $hobbies = $('#hobbies', $user);
	var $rels = $('#relationships', $user);

	this.xhr = $.ajax({
		url : serviceUri + "/services?url=" + params.uri + link,
		type : 'GET',
		dataType : 'json',
		success : function(data, textStatus, XMLHttpRequest) {
			$nick.html(data.name);
			$hobbies.html(data.hobbies);

			var items = [];
			$.each(data.relationships, function(i, item) {
				items.push($('<li>', {
					text : item.nick + " [" + item.relationtype + "]"
				}));
			});
			$rels.html(items);

			$user.show();
		},
		error : function(jqXHR, ajaxOptions, thrownError) {
			showAjaxError(jqXHR, ajaxOptions, thrownError);

			$user.hide();
		}
	});
}


function getUser2(nick, teamId) {
	var params = options[teamId];

	var $user = $('#affiliate-user');
	var $nick = $('#nick', $user);
	var $hobbies = $('#hobbies', $user);
	var $rels = $('#relationships', $user);

	this.xhr = $.ajax({
		url : serviceUri + "/services?url=" + params.uri
				+ params.services.user.replace('{id}', nick),
		type : 'GET',
		dataType : 'json',
		success : function(data, textStatus, XMLHttpRequest) {
			$nick.html(data.nick);
			$hobbies.html(data.hobbies);

			var items = [];
			$.each(data.relationships, function(i, item) {
				items.push($('<li>', {
					text : item.nick + " [" + item.relationtype + "]"
				}));
			});
			$rels.html(items);

			$user.show();
		},
		error : function(jqXHR, ajaxOptions, thrownError) {
			showAjaxError(jqXHR, ajaxOptions, thrownError);

			$user.hide();
		}
	});
}
